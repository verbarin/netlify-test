const path = require(`path`)

if (process.env.STAGING) {
  require("dotenv").config({
    path: `.env.${process.env.NODE_ENV}.staging`,
  })
} else {
  require("dotenv").config({
    path: `.env.${process.env.NODE_ENV}`,
  })
}

const buildEnv = process.env.BUILD_ENV ? process.env.BUILD_ENV.trim() : false
const buildEnvProduction = buildEnv ? buildEnv == "production" : false

module.exports = {
  siteMetadata: {
    title: `Upfinity.io`,
    author: {
      name: `Upfinity`,
      summary: `This is an Upfinity's website`,
    },
    description: `Basic description`,
    siteUrl: `https://upfinity.io/`,
    social: {
      twitter: `upfinity`,
    },

    endpoints: {
      base: 'https://upfinity-proxy.herokuapp.com/cl/',
      log: 'https://upfinity-proxy.herokuapp.com/cl/logs/',
      free_pdf: 'https://app.upfinity.io/free_pdf',
      buy_entre: 'https://app.upfinity.io/buy_entre',
      buy_request: 'https://app.upfinity.io/buy_request',
      buy_enterprise: 'https://app.upfinity.io/buy_enterprise',

      signin: 'https://app.upfinity.io/signin?some_default_param=some_value',
      signup: 'https://app.upfinity.io/signup?some_default_param=some_value',
      purchase: 'https://app.upfinity.io/purchase?some_default_param=some_value',
      subscribe: 'https://app.upfinity.io/subscribe?some_default_param=some_value',
      enterprise: 'https://app.upfinity.io/enterprise?some_default_param=some_value',
      soup: 'https://app.upfinity.io/soup?some_default_param=some_value',
      formcarry: 'https://formcarry.com/s/1rYl6zQ-d_1z',
      en: {
        email: 'feedback@upfinity.io',
        linkedin: 'https://www.linkedin.com/company/upfinity',
        facebook: 'https://www.facebook.com/upfinityglobal/'
      },
      ru: {
        email: 'feedback@upfinity.io',
        linkedin: 'https://www.linkedin.com/company/upfinity',
        facebook: 'https://www.facebook.com/upfinity'
      }
    },

    members: [
      {
        hash: '#denis-anoshin',
        ru: {
          name: 'Денис Аношин',
          title: 'Со-основатель,  CEO',
          photo: `../../anoshin.png`,
          color: `rgba(17, 175, 164, 0.15)`,
          contacts: `../../vcf/rus-denis.vcf`,
          details: {
            title: 'Со-основатель, CEO',
            links: [
              {
                icon: '../../linkedin.png',
                url: 'https://www.linkedin.com/in/denis-anoshin-cfa-428b2b49/'
              },
              {
                icon: '../../fb.png',
                url: 'https://www.facebook.com/denis.anoshin'
              }
            ],
            about: 'В качестве генерального директора Денис играет важную роль в стратегическом и операционном управлении компанией. Кроме того, он лично возглавляет проект по разработке критически важного модуля ИИ. Денис обладает 10-летним опытом в инвестиционном банкинге и 4-летним опытом в консалтинге. Недавно он получил степень Executive MBA в HEC, Париж.'
          }
        },
        en: {
          name: 'Denis Anoshin',
          title: 'Co-founder, CEO',
          photo: `../../anoshin.png`,
          color: `rgba(17, 175, 164, 0.15)`,
          contacts: `../../vcf/eng-denis.vcf`,
          details: {
            title: 'Co-founder, CEO',
            links: [
              {
                icon: '../../linkedin.png',
                url: 'https://www.linkedin.com/in/denis-anoshin-cfa-428b2b49/'
              },
              {
                icon: '../../fb.png',
                url: 'https://www.facebook.com/denis.anoshin'
              }
            ],
            about: 'As an acting CEO Denis plays important role both in strategic and operational management of the firm. In addition he personally leads development of the crucial AI module. Denis has 10 years experience in investment banking and 4 years in consulting. Recently he earned Executive MBA degree in HEC, Paris.'
          }
        }
      },
      {
        hash: '#sergei-kouzmin',
        ru: {
          name: 'Сергей Кузьмин',
          title: 'Со-основатель, CPO',
          photo: `../../kuzmin.png`,
          color: `rgba(11,120,191,0.15)`,
          contacts: `../../vcf/rus-serge.vcf`,
          details: {
              title: 'Со-основатель, CPO',
              links: [
                {
                  icon: '../../linkedin.png',
                  url: 'https://www.linkedin.com/in/sergei-kouzmin-0156251a9/'
                },
                {
                  icon: '../../fb.png',
                  url: 'https://www.facebook.com/sergey.kouzmin.3'
                }
              ],
              about: 'В Upfinity Сергей занимается разработкой новых продуктов, а также участием в  общем управлении компанией. До Upfinity Сергей работал в инвестиционных блоках крупнейших российских банков, где организовал ряд сделок по структурному финансированию общим объемом более $1 млрд. Сергей является выпускником Высшей Школы Экономики и Лондонской Школы Экономики.'
            }
        },
        en: {
          name: 'Sergei Kouzmin',
          title: 'Co-founder, CPO',
          photo: `../../kuzmin.png`,
          color: `rgba(11,120,191,0.15)`,
          contacts: `../../vcf/eng-serge.vcf`,
          details: {
              title: 'Co-founder, CPO',
              links: [
                {
                  icon: '../../linkedin.png',
                  url: 'https://www.linkedin.com/in/sergei-kouzmin-0156251a9/'
                },
                {
                  icon: '../../fb.png',
                  url: 'https://www.facebook.com/sergey.kouzmin.3'
                }
              ],
              about: 'At Upfinity Sergei is focused on new product concept development and also takes part in overall management of the firm. Before Upfinity, Sergei worked at investment divisions of large Russian banks, where he was responsible for originating a series of structured finance deals amounting in total to more than $1 billion. Sergei holds a degree from Higher School of Economics as well as London School of Economics.'
            }
        }
      },
      {
        hash: '#maksim-sitov',
        ru: {
          name: 'Максим Ситов',
          title: 'Со-основатель, COO',
          photo: `../../sitov.png`,
          color: `rgba(214,238,237,1)`,
          contacts: `../../vcf/rus-max.vcf`,
          details: {
            title: 'Со-основатель, COO',
            links: [
              {
                icon: '../../linkedin.png',
                url: 'https://www.linkedin.com/in/maksim-sitov-4bb36944/'
              },
              {
                icon: '../../fb.png',
                url: 'https://www.facebook.com/max.sitov'
              }
            ],
            about: 'Окончил мехмат МГУ. В течение 8 лет занимался производными финансовыми инструментами. В Upfinity работает прежде всего над операционной и технической частью.'
          }
        },
        en: {
          name: 'Maksim Sitov',
          title: 'Co-founder, COO',
          photo: `../../sitov.png`,
          color: `rgba(214,238,237,1)`,
          contacts: `../../vcf/eng-max.vcf`,
          details: {
            title: 'Co-founder, COO',
            links: [
              {
                icon: '../../linkedin.png',
                url: 'https://www.linkedin.com/in/maksim-sitov-4bb36944/'
              },
              {
                icon: '../../fb.png',
                url: 'https://www.facebook.com/max.sitov'
              }
            ],
            about: 'Graduated from Moscow State University with specialization in Mathematics. Worked in financial derivatives for 8 years. At Upfinity focuses primarily on operational and technical stuff.'
          }
        }
      },
      {
        hash: '#vitaly-novikov',
        en: {
          name: 'Vitaly Novikov',
          title: 'Co-founder',
          photo: `../../novikov.png`,
          color: `#F5DCE6`,
          details: {
            title: 'Co-founder',
            links: [
              {
                icon: '../../linkedin.png',
                url: 'https://www.linkedin.com/in/vitaly-novikov/'
              },
              {
                icon: '../../fb.png',
                url: 'https://www.facebook.com/vitaly.newman'
              }
            ],
            about: 'Graduated from BMSTU, “Rocket-science engineer” specialisation. Certified Professional in Business Analysis, PMI-PBA and Project Management Professional, PMP.'
          }
        },
        ru: {
          name: 'Виталий Новиков',
          title: 'Со-основатель',
          photo: `../../novikov.png`,
          color: `#F5DCE6`,
          details: {
            title: 'Со-основатель',
            links: [
              {
                icon: '../../linkedin.png',
                url: 'https://www.linkedin.com/in/vitaly-novikov/'
              },
              {
                icon: '../../fb.png',
                url: 'https://www.facebook.com/vitaly.newman'
              }
            ],
            about: 'Окончил МГТУ им. Баумана, инженер-конструктор по специальности “Ракетостроение” и специализации “Крылатые ракеты”. Сертифицированный профессионал в бизнес-анализе PMI-PBA и профессиональный руководитель проектов PMP.'
          }
        }
      },
      {
        hash: '#danila-ivanchenko',
        ru: {
          name: 'Данила Иванченко',
          title: 'CMO',
          photo: `../../danila.png`,
          color: `rgba(17, 175, 164, 0.15)`,
          contacts: `../../vcf/rus-danila.vcf`,
          details: {
            title: 'CMO',
            links: [
              {
                icon: '../../linkedin.png',
                url: 'https://www.linkedin.com/in/danila-ivanchenko-47b621a6/'
              },
              {
                icon: '../../fb.png',
                url: 'https://www.facebook.com/danila.ivanchenko.1'
              }
            ],
            about: 'Данила окончил кафедру маркетинга в РАНХиГС, занимался кросс-медийным планированием для клиентов и развитием Adtech решений в международных агентствах GroupM и BBDO. Сейчас Данила отвечает за формирование и реализацию маркетинговой стратегии Upfinity.'
          }
        },
        en: {
          name: 'Danila Ivanchenko',
          title: 'CMO',
          photo: `../../danila.png`,
          color: `rgba(17, 175, 164, 0.15)`,
          contacts: `../../vcf/eng-danila.vcf`,
          details: {
            title: 'CMO',
            links: [
              {
                icon: '../../linkedin.png',
                url: 'https://www.linkedin.com/in/danila-ivanchenko-47b621a6/'
              },
              {
                icon: '../../fb.png',
                url: 'https://www.facebook.com/danila.ivanchenko.1'
              }
            ],
            about: 'Danila has a marketing degree at Russian Presidential Academy of National Economy and Public Administration. Engaged in cross-media planning for clients and development of Adtech solutions at international agencies GroupM and BBDO. Danila is now responsible for the formation and implementation of Upfinity’s marketing strategy.'
          }
        }
      },
      {
        hash: '#dmytryj-velykorodnyj',
        en: {
          name: 'Dmytryj Velikorodnyj',
          title: 'Back-end developer',
          photo: `../../velikorodnyj.png`,
          color: `#F8E3DD`,
          details: {
            title: 'Back-end developer',
            links: [
              {
                icon: '../../linkedin.png',
                url: 'https://www.linkedin.com/in/dmytryj-velykorodnyj-490939173/'
              }
            ],
            about: 'At Upfinity Dmytryj is developing and supporting various internal services, starting from a primary app and ending with Upfinity’s AI. He previously worked at a web applications vendor company where he took part in multiple international projects. Dmytryj has more than 6 years of experience in back-end development.'
          }
        },
        ru: {
          name: 'Dmytryj Velikorodnyj',
          title: 'Back-end разработчик',
          photo: `../../velikorodnyj.png`,
          color: `#F8E3DD`,
          details: {
            title: 'Back-end разработчик',
            links: [
              {
                icon: '../../linkedin.png',
                url: 'https://www.linkedin.com/in/dmytryj-velykorodnyj-490939173/'
              }
            ],
            about: 'В Upfinity Дмитрий развивает и поддерживает внутренние сервисы, начиная от основного приложения и заканчивая Искусственным Интеллектом. Ранее работал в компании-вендоре веб-приложений, за плечами участие в развитии нескольких международных проектов. Опыт back-end разработки — более 6 лет.'
          }
        }
      },
      {
        hash: '#stanislav-baturin',
        en: {
          name: 'Stanislav Baturin',
          title: 'Front-end developer',
          photo: `../../baturin.png`,
          color: `#D6EEED`,
          details: {
            title: 'Front-end developer',
            links: [
              {
                icon: '../../linkedin.png',
                url: 'https://www.linkedin.com/in/stanislav-baturin/'
              }
            ],
            about: 'At Upfinity Stanislav is developing web applications. Prior to that, Stanislav was involved in the development of applications for the fintech industry. General development experience: 8 years. Graduated from Tomsk State University of Control Systems and Radioelectronics. Engineer\'s degree, “Software for computers and automated systems 230105”.'
          }
        },
        ru: {
          name: 'Станислав Батурин',
          title: 'Front-end разработчик',
          photo: `../../baturin.png`,
          color: `#D6EEED`,
          details: {
            title: 'Front-end разработчик',
            links: [
              {
                icon: '../../linkedin.png',
                url: 'https://www.linkedin.com/in/stanislav-baturin/'
              }
            ],
            about: 'В Upfinity Станислав занимается разработкой веб-приложений. До этого Станислав занимался разработкой приложений для финтех отрасли. Общий опыт разработки: 8 лет. Окончил ТУСУР по специальности “Программное обеспечение ВТ и АС“.'
          }
        }
      },
      {
        hash: '#viachaslau-viarbitski',
        en: {
          name: 'Viachaslau Viarbitski',
          title: 'UX/UI Engineer',
          photo: `../../verbarin.png`,
          color: `#D5E6F1`,
          details: {
            title: 'UX/UI Engineer',
            links: [
              {
                icon: '../../linkedin.png',
                url: 'https://www.linkedin.com/in/vvverbitski/?locale=en_US'
              }
            ],
            about: 'At Upfinity Viachaslau is researching User Experience and is designing interfaces. Has over 7 years experience in developing complex platforms and applications.'
          }
        },
        ru: {
          name: 'Вячеслав Вербицкий',
          title: 'UX/UI инженер',
          photo: `../../verbarin.png`,
          color: `#D5E6F1`,
          details: {
            title: 'UX/UI инженер',
            links: [
              {
                icon: '../../linkedin.png',
                url: 'https://www.linkedin.com/in/vvverbitski/'
              }
            ],
            about: 'В Upfinity занимается исследованием пользовательского опыта и проектированием интерфейсов. До этого 7 лет занимался разработкой сложных платформ и приложений.'
          }
        }
      },
      {
        hash: '#ekaterina-anplitova',
        en: {
          name: 'Ekaterina Anplitova',
          title: 'Front-end developer',
          photo: `../../anplitova.png`,
          color: `#d0bc88`,
          details: {
            title: 'Front-end developer',
            links: [
              {
                icon: '../../linkedin.png',
                url: 'https://www.linkedin.com/in/anplitova/'
              },
              {
                icon: '../../fb.png',
                url: 'https://www.facebook.com/anplitova'
              }
            ],
            about: 'At Upfinity Ekaterina is supporting and developing web applications. Prior to that, she was involved in the development of sites and web applications for a large online store. General development experience: 7 years. Graduated from Volgograd State Technical University. Engineer\'s degree, “230101: Computing machines, complexes, systems and networks”.'
          }
        },
        ru: {
          name: 'Екатерина Анплитова',
          title: 'Front-end разработчик',
          photo: `../../anplitova.png`,
          color: `#d0bc88`,
          details: {
            title: 'Front-end разработчик',
            links: [
              {
                icon: '../../linkedin.png',
                url: 'https://www.linkedin.com/in/anplitova/'
              },
              {
                icon: '../../fb.png',
                url: 'https://www.facebook.com/anplitova'
              }
            ],
            about: 'В Upfinity Екатерина занимается поддержкой и разработкой веб-приложений. До этого занималась разработкой сайтов и веб-приложений для крупного интернет-магазина. Общий опыт разработки: 7 лет. Окончила Волгоградский государственный технический университет по специальности "230101: Вычислительные машины, комплексы, системы и сети".'
          }
        }
      }
    ]
  },
  plugins: [
    {
      resolve: `gatsby-plugin-sitemap`,
      options: {
        query: `{
          site {
            siteMetadata {
              siteUrl
            }
          }
          allSitePage {
            nodes {
              path
            }
          }
        }`,
        serialize: ({ site, allSitePage }) =>
          allSitePage.nodes
            .filter(node => {
              const path = node.path
              // Filter out 404 pages
              if (path.includes("404")) {
                return false
              }
              // Filter out base pages that don't have a language directory
              return [`en`, `ru`].includes(path.split("/")[1])
            })
            .map(node => {
              return {
                url: `${site.siteMetadata.siteUrl}${node.path}`,
                changefreq: `weekly`,
                priority: 0.7,
              }
            }),
      }
    },
    {
      resolve: `${__dirname}/src/plugins/gatsby-plugin-fbchat`,
      options: {
        sdk: {
          appId: '832640374046099',
          xfbml: '1',
          version: 'v10.0'
        },
        chat: {
          pageId: '100338055322813',
          loggedInGreeting: 'Привет! Чем помочь?',
          loggedOutGreeting: 'Привет! Чем помочь?',
        }
      },
    },
    {
      resolve: require.resolve(`${__dirname}/src/plugins/gatsby-plugin-google-tagmanager-custom`),
      options: {
        id: "GTM-5RX4W9Q",
        includeInDevelopment: false,
        includeOnlyProduction: buildEnvProduction
      },
    },
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        path: `${__dirname}/content/blog`,
        name: `blog`,
      },
    },
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        path: `${__dirname}/static/img/uploads`,
        name: `images`,
      },
    },
    {
      resolve: `gatsby-mdx`,
      options: {
        extensions: ['.mdx', '.md'],
        gatsbyRemarkPlugins: [
          {
            resolve: `gatsby-remark-images`,
            options: {
              maxWidth: 1200,
              sizeByPixelDensity: true,
              withWebp: true,
              showCaptions: ['title'],
              markdownCaptions: true,
              wrapperStyle: fluidResult => `flex:${_.round(fluidResult.aspectRatio, 2)};`
            }
          }
        ]
      }
    },
    {
      resolve: `gatsby-transformer-remark`,
      options: {
        plugins: [
          {
            resolve: `gatsby-remark-images`,
            options: {
              maxWidth: 1200,
              showCaptions: ['title'],
              markdownCaptions: true,
              sizeByPixelDensity: true,
              withWebp: true,
              wrapperStyle: fluidResult => `just-prop: 0;`
            },
          },
          {
            resolve: `gatsby-remark-responsive-iframe`,
            options: {
              wrapperStyle: `margin-bottom: 1.0725rem`,
            },
          },
          `gatsby-remark-prismjs`,
          `gatsby-remark-copy-linked-files`,
          `gatsby-remark-smartypants`,
        ],
      },
    },
    `gatsby-transformer-sharp`,
    `gatsby-plugin-sharp`,
    `gatsby-plugin-feed`,
    {
      resolve: `gatsby-plugin-manifest`,
      options: {
        name: `Gatsby Starter Blog`,
        short_name: `GatsbyJS`,
        start_url: `/en`,
        background_color: `#ffffff`,
        theme_color: `#663399`,
        display: `minimal-ui`,
        icon: `content/assets/gatsby-icon.png`,
      },
    },
    `gatsby-plugin-react-helmet`,
    // this (optional) plugin enables Progressive Web App + Offline functionality
    // To learn more, visit: https://gatsby.dev/offline
    // `gatsby-plugin-offline`,
    {
      resolve: `gatsby-plugin-intl`,
      options: {
        // language JSON resource path
        path: `${__dirname}/src/intl`,
        // supported language
        languages: [`en`, `ru`],
        // language file path
        defaultLanguage: `en`,
        // option to redirect to `/en` when connecting `/`
        redirect: true,
      },
    },
    {
      resolve: 'gatsby-plugin-react-svg',
      options: {
        rule: {
          include: path.resolve(__dirname, 'src/assets/svg')
        },
      },
    },
    {
      resolve: `gatsby-plugin-sass`
    },
    `gatsby-plugin-netlify-cms`
  ],
}

import "./src/styles/global.scss"
import wrapWithProvider from './src/state/ReduxWrapper'
export const wrapRootElement = wrapWithProvider
const ReactDOM = require('react-dom')

export function replaceHydrateFunction() {
    return (element, container, callback) => {
        ReactDOM.render(element, container, callback)
    }
}
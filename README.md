Весь сайт построен на базе Gatsby (gatsby-starte-blog)

Официальная документация по Gatsby и стартер-паку [official and community-created starters](https://www.gatsbyjs.com/docs/gatsby-starters/).



## 🚀 Команды

**Локальный dev-сервер**

Во время локальной разработки работает hot-сервер

```shell
# start dev-server
gatsby develop
```

**Очистить кэш**

Очистка кэша требуется, например, при внесении в файлы локализации

```shell
# clear cashe
gatsby clean
```
    
**Собрать билд**

При корректной настройке BitBucket + Netlify билд происходит автоматически

```shell
# Compile the project
gatsby build
```


## 🧐 Что внутри?

Структура проекта и работа с файлами

    .
    ├── node_modules
    ├── content
    └──── blog # здесь хранятся статьи блога
    ├── public # сюда происходит сборка проекта и эта папка "смотрит" в веб
    ├── static # здесь храняться медиа-файлы: изображения, видео, иконки и svg, которым не нужен контроль
    ├── src
    └──── assets\svg # здесь хранятся svg-файлы, которые нужны в качестве компонентов
    ├── .gitignore
    ├── .prettierrc
    ├── gatsby-browser.js
    ├── gatsby-config.js # главный конфиг. Тут указываются эндпоинты, базовые мета, члены команды, локали сайта
    ├── gatsby-node.js
    ├── LICENSE
    ├── package-lock.json
    ├── package.json
    └── README.md

1.  **`/node_modules`**: This directory contains all of the modules of code that your project depends on (npm packages) are automatically installed.

2.  **`/src`**: This directory will contain all of the code related to what you will see on the front-end of your site (what you see in the browser) such as your site header or a page template. `src` is a convention for “source code”.

3.  **`.gitignore`**: This file tells git which files it should not track / not maintain a version history for.

4.  **`.prettierrc`**: This is a configuration file for [Prettier](https://prettier.io/). Prettier is a tool to help keep the formatting of your code consistent.

5.  **`gatsby-browser.js`**: This file is where Gatsby expects to find any usage of the [Gatsby browser APIs](https://www.gatsbyjs.com/docs/browser-apis/) (if any). These allow customization/extension of default Gatsby settings affecting the browser.

6.  **`gatsby-config.js`**: This is the main configuration file for a Gatsby site. This is where you can specify information about your site (metadata) like the site title and description, which Gatsby plugins you’d like to include, etc. (Check out the [config docs](https://www.gatsbyjs.com/docs/gatsby-config/) for more detail).

7.  **`gatsby-node.js`**: This file is where Gatsby expects to find any usage of the [Gatsby Node APIs](https://www.gatsbyjs.com/docs/node-apis/) (if any). These allow customization/extension of default Gatsby settings affecting pieces of the site build process.

9.  **`LICENSE`**: This Gatsby starter is licensed under the 0BSD license. This means that you can see this file as a placeholder and replace it with your own license.

10. **`package-lock.json`** (See `package.json` below, first). This is an automatically generated file based on the exact versions of your npm dependencies that were installed for your project. **(You won’t change this file directly).**

11. **`package.json`**: A manifest file for Node.js projects, which includes things like metadata (the project’s name, author, etc). This manifest is how npm knows which packages to install for your project.

12. **`README.md`**: A text file containing useful reference information about your project.

## 🎓 Официальная документация по Gatsby

Looking for more guidance? Full documentation for Gatsby lives [on the website](https://www.gatsbyjs.com/). Here are some places to start:

- **For most developers, we recommend starting with our [in-depth tutorial for creating a site with Gatsby](https://www.gatsbyjs.com/tutorial/).** It starts with zero assumptions about your level of ability and walks through every step of the process.

- **To dive straight into code samples, head [to our documentation](https://www.gatsbyjs.com/docs/).** In particular, check out the _Guides_, _API Reference_, and _Advanced Tutorials_ sections in the sidebar.

## 💫 Кейсы

### **Как интегрировать админку с Netlify CMS**

Подробный туториал можно посмотреть тут [https://www.youtube.com/watch?v=IWmVSm2KevY](https://www.youtube.com/watch?v=IWmVSm2KevY)

### **Замена видео**

Для замены видео необходимо заменить одноименный файл ("video.mp4" для главной страницы) и "закомитить" в репозиторий. Аналогичные действия для замены видео в "How it works"

### **Члены команды**

Для добавления/редактирования членов команды, надо внести изменения в **`gatsby-config.js`**

```shell
[
...,
	{
        name: 'Denis Anoshin', # Имя члена
        title: 'CEO', # Должно сокращенно
        photo: `../../anoshin.png`, # Путь до фото
        color: `rgba(11,120,191,0.15)`, # Цвет подложки
        details: {
            title: 'Chief Executive Officer', # Должность полностью
            links: [
              {
                icon: '../../linkedin.png', # Путь к иконке для ссылки
                url: 'https://google.com' # Ссылка
              }
            ],
            about: 'Lorem ipsum' # Описание члена
        },
    },
...
]
```

### **Ссылки авторизации и кнопок**

При нажатии на "Sign In", "Sign Up", "Try Demo for Free", "Purchase", "Subscribe" и "Submit" в Энтерпрайз-форме происходит редирект пользователя на указанный URL. Данные URL находятся и редактируются в **`gatsby-config.js`**

```shell
	endpoints: {
      signin: 'https://app.upfinity.io/signin?some_default_param=some_value',
      signup: 'https://app.upfinity.io/signup?some_default_param=some_value',
      purchase: 'https://app.upfinity.io/purchase?some_default_param=some_value',
      subscribe: 'https://app.upfinity.io/subscribe?some_default_param=some_value',
      enterprise: 'https://app.upfinity.io/enterprise?some_default_param=some_value',
      soup: 'https://app.upfinity.io/soup?some_default_param=some_value'
    },
```

Для enterprise во время сабмита добавляются следующие параметры:

```shell
&first_name=... <String> # Имя
&last_name=... <String> # Фамилия
&phone=... <String> # Указанный телефон
&company_name=... <String> # Введенное название компании
&country=... <String> # Выбранная страна
&email=... <String> # Указанный Email

Example:
https://app.upfinity.io/enterprise?some_default_param=some_value&first_name=John&last_name=Doe&phone=3123512124&company_name=Google&country=Japan&email=test@test.com
```

### **Сбор имейлов и ссылки на социальные сети**

Сбор имейлов для рассылки осуществляется на FormCarry. Изменить адрес ссылки можно в файле **`gatsby-config.js`**

```shell
formcarry: 'https://formcarry.com/s/1rYl6zQ-d_1z'
```

Тут же можно изменить адреса ссылок на социальные сети LinkedIn и Facebook

### **Написание и редактирование статей блога**

Написание/редактирование статей блога осуществляется по адресы "https://<website-domain>/admin". При этом доступ осуществляется через BitBucket-аккаунт, у которого есть доступ к репозиторию с правом комитов.

**`Администратор публикует статью/изменения -> Netlify CMS осуществляет комит -> Netlify CMS получает сигнал о новом комите и запускает gatsby build`**

Публикация статьи и ребилд занимают около 5-10 минут

### **Замена Favicon**

Для замены Favicon необходимо заменить файл **`content\assets\gatsby-icon.png`** на аналогичный с разрешением не менее 512x512px
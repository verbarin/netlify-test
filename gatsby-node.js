const path = require(`path`)
const { createFilePath } = require(`gatsby-source-filesystem`)
const kebabCase = require(`lodash.kebabcase`)

const blogPosts = new Map()
const categories = []
const tags = []

let posts

const blogTemplate = path.resolve(`./src/pages/blog.jsx`)

exports.createPages = async ({ graphql, actions, reporter }) => {
  const { createPage } = actions

  const blogPostTemplate = path.resolve(`./src/templates/blog-post.jsx`)
  const blogCategoryTemplate = path.resolve(`./src/templates/blog-category.jsx`)
  const blogTagTemplate = path.resolve(`./src/templates/blog-tag.jsx`)

  const result = await graphql(
    `
      {
        allMarkdownRemark(
          sort: { fields: [frontmatter___date], order: DESC }
          limit: 1000
        ) {
          nodes {
            id
            fields {
              slug
              locale
              type
            }
            frontmatter {
              date
              locale
              title
              type
              thumbnail
              category
              tags
            }
          }
        }
      }
    `
  )

  if (result.errors) {
    reporter.panicOnBuild(
      `There was an error loading your blog posts`,
      result.errors
    )
    return
  }

  posts = result.data.allMarkdownRemark.nodes

  if (posts.length > 0) {
    posts.forEach((post, index) => {
      const previousPostId = index === 0 ? null : posts[index - 1].id
      const nextPostId = index === posts.length - 1 ? null : posts[index + 1].id

      blogPosts.set(post.fields.slug, {})

      if(post.frontmatter && post.frontmatter.category) {
        post.frontmatter.category.forEach(cat => categories.push({locale: post.frontmatter.locale, cat: cat}))
      }

      if(post.frontmatter && post.frontmatter.tags) {
        post.frontmatter.tags.forEach(tag => tags.push(tag))
      }
      
      createPage({
        path: `blog/article${post.fields.slug}`,
        component: blogPostTemplate,
        context: {
          id: post.id,
          locale: post.frontmatter.locale,
          type: post.frontmatter.type,
          previousPostId,
          nextPostId,
        },
      })
    })

    const countTags = tags.reduce((prev, curr) => {
      prev[curr] = (prev[curr] || 0) + 1
      return prev
    }, {})
    
    allTags = Object.keys(countTags)
    allTags.forEach((tag, i) => {
      const link = `/blog/tag/${kebabCase(tag)}`
      const postsPerPage = 10
    
      Array.from({
        length: Math.ceil(countTags[tag] / postsPerPage),
      }).forEach((_, i) => {
        createPage({
          path: i === 0 ? link : `${link}/page/${i + 1}`,
          component: blogTagTemplate,
          context: {
            allTags: allTags,
            slug: kebabCase(tag),
            tags: tag,
            limit: postsPerPage,
            skip: i * postsPerPage,
            currentPage: i + 1,
            numPages: Math.ceil(countTags[tag] / postsPerPage),
          },
        })
      })
    })

    /**
     * 
     */

    const countCategories = categories.reduce((prev, curr) => {
      prev[curr.cat] = (prev[curr.cat] || 0) + 1
      return prev
    }, {})
    
    allCategories = Object.keys(countCategories);
    allCategories.forEach((cat, i) => {
      const link = `/blog/category/${kebabCase(cat)}`
      const postsPerPage = 10
    
      Array.from({
        length: Math.ceil(countCategories[cat] / postsPerPage),
      }).forEach((_, i) => {
        createPage({
          path: i === 0 ? link : `${link}/page/${i + 1}`,
          component: blogCategoryTemplate,
          context: {
            allCategories: allCategories.filter((thing, index, self) =>
              index === self.findIndex((t) => (
                t.locale === thing.locale && t.cat === thing.cat
              ))
            ),
            slug: kebabCase(cat),
            category: cat,
            limit: postsPerPage,
            skip: i * postsPerPage,
            currentPage: i + 1,
            numPages: Math.ceil(countCategories[cat] / postsPerPage),
          },
        })
      })
    })
  }
}

exports.onCreatePage = async ({ page, actions }) => {
  const { createPage, deletePage } = actions

  if(page.context.type &&  page.context.type === "blog") {
    deletePage(page)

    if(page.context.locale === page.context.language) {
      createPage({
        ...page,
        path: `${page.context.locale}${page.context.intl.originalPath}`
      })
    }
  }

  if(page.context.intl.originalPath === "/blog/") {
    deletePage(page)

    //
    const postsPerPage = 9
    const numPages = Math.ceil(posts.filter(post => post.frontmatter.locale === page.context.language).length / postsPerPage)

    Array.from({ length: numPages }).forEach((_, i) => {
      createPage({
        ...page,
        path: i === 0 ? page.path : `${page.path}page/${i + 1}`,
        component: blogTemplate,
        context: {
          ...page.context,

          allCategories: categories.filter((thing, index, self) =>
            index === self.findIndex((t) => (
              t.locale === thing.locale && t.cat === thing.cat
            ))
          ).map(item => {
            return {
              locale: item.locale,
              title: item.cat,
              slug: kebabCase(item.cat)
            }
          }),
          allTags: tags.filter((item, index, self) => (self.indexOf(item) === index)).map(item => {
            return {
              title: item,
              slug: kebabCase(item)
            }
          }),
          
          limit: postsPerPage,
          skip: i * postsPerPage,
          currentPage: i + 1,
          numPages: numPages,
        },
      })
    })
    //

    // createPage({
    //   ...page,
    //   context: {
    //     ...page.context,
    //     allCategories: categories.filter((item, index, self) => (self.indexOf(item) === index)).map(item => {
    //       return {
    //         title: item,
    //         slug: kebabCase(item)
    //       }
    //     }),
    //     allTags: tags.filter((item, index, self) => (self.indexOf(item) === index)).map(item => {
    //       return {
    //         title: item,
    //         slug: kebabCase(item)
    //       }
    //     })
    //   }
    // })
  }
}

exports.onCreateNode = ({ node, actions, getNode }) => {
  const { createNodeField } = actions

  if (node.internal.type === `MarkdownRemark`) {
    const value = createFilePath({ node, getNode })

    const fileNode = getNode(node.parent)
    const type = fileNode.sourceInstanceName

    createNodeField({
      name: `slug`,
      node,
      value: `${value}`,
    })
    createNodeField({
      name: `type`,
      node,
      value: type,
    })
  }
}

exports.createSchemaCustomization = ({ actions }) => {
  const { createTypes } = actions

  createTypes(`
    type SiteSiteMetadata {
      author: Author
      siteUrl: String
      social: Social
    }

    type Author {
      name: String
      summary: String
    }

    type Social {
      twitter: String
    }

    type MarkdownRemark implements Node {
      frontmatter: Frontmatter
      fields: Fields
    }

    type Frontmatter {
      title: String
      description: String
      thumbnail: String
      locale: String
      type: String
      date: Date @dateformat
    }

    type Fields {
      slug: String
      locale: String
      type: String
    }
  `)
}

exports.onCreateWebpackConfig = ({ stage, loaders, actions }) => {
  actions.setWebpackConfig({
    resolve: {
        fallback: {
          "crypto": false,
          "stream": false
        },
      },
    })
}
---
locale: en
title: How to test tech startup ideas
thumbnail: ../../../../img/blog-1-.png
date: 2021-04-21T13:49:09.688Z
description: 2020s break records in tech startups growth statistics. According
  to PwC and CB Insights' Q3 2020 report, VC investments to US-based, VC-backed
  companies hit a 7-quarter high in Q3’20 at $36.5B, up 22% year-over-year (YoY)
  and 30% from Q2’20.
category:
  - business
tags:
  - idea
  - startups
  - validation
---
If you already have a brilliant IT startup idea and aim to launch a new product onto a highly competitive market, the only thing left to do is hypothesis testing.

The relevance of this point will hopefully appear more clearly below.

## **State of the global startup economy**

*«The world has been underestimating the technology market size. But nowadays more and more people are realizing this»*, stated Roseanne Wincek, Co-Founder & Managing Director of Renegade Partners.

2020s break records in tech startups growth statistics. According to PwC and CB Insights' Q3 2020 [report](https://www.cbinsights.com/research/report/venture-capital-q3-2020/), VC investments to US-based, VC-backed companies hit a 7-quarter high in Q3’20 at $36.5B, up 22% year-over-year (YoY) and 30% from Q2’20. Other countries’ figures are lower but still record-breaking. Venture backing for EU tech reached a monthly record of $5 billion in September, putting a high of $41 billion for 2020 at hand, the [study](https://2020.stateofeuropeantech.com/chapter/state-european-tech-2020/article/exec-sum/) of venture capital firm Atomico says. Such a year’s total is a $500 million more than in 2019. As for Russian startups, investments were 3 times higher in 2020 than in 2019, as claimed by Inc. Magazine. Besides, Russia [saw](https://incrussia.ru/news/russian-venture-capital-market/) a twofold increase in venture funding.

However, business development is always associated with numerous risks. Early-stage startups, obviously, face the hardest challenges and have the greatest failure rates: the quoted number is that 9 out of 10 startups fail. Addressing issues that are interesting to solve rather than those that serve a market need was cited as the №1 [reason](https://www.cbinsights.com/research/startup-failure-reasons-top/) for failure, marked in 42% of cases. At the next stage, over 70% of startups crash [because](https://www.forbes.com/sites/abdoriani/) of premature scaling. Only a tiny fraction of all new businesses survives bringing in revenue to their founders.

Right goal setting is halfway to success, especially in the era of extreme uncertainty.

## **Idea validation**

The ultimate goal of any startup is to meet market demands by launching innovative product. Entrepreneur’s offer has to intrigue the target audience and exceed its expectations. The value and relevance of the product can be checked through idea validation. Idea validation is the process of testing if a concept is economically feasible.Testing can help minimize the risk of losing resources, analyze the current industry situation, and also point out a clear direction for further development.

Here are a few ways to test new business hypotheses:

### **Conversation with potential customers**

An interesting way to evaluate the viability of a product idea is to interview relatives, friends, colleagues, neighbors, and even strangers. Suchlike quantitative research can be made independently or using special online tools – e.g., [Survey](https://www.surveymonkey.com/), [Typeform](https://www.typeform.com/surveys/), [Alchemer](https://www.alchemer.com/), [Crowdsignal](https://crowdsignal.com/). The undoubted advantage of this method is that it allows you to make plenty of unexpected discoveries at low cost. However, there are some nuances: family members' responses may be somewhat biased, and the chance to find target customers among random respondents is comparatively rare. Friendly reminder: be careful not to let others steal and implement your excellent startup idea.

### **Market research**

Alternatively, there is an option to conduct a deeper market analysis in order to determine the appropriate niche, scan competitive environment, and, perhaps, get inspired by peer experience. Follow the steps outlined below:

* look through Startup Databases on ad hoc websites – e.g., [Crunchbase.com](https://www.crunchbase.com/), [EU-Startups.com](https://www.eu-startups.com/), [Dealroom.co](https://dealroom.co/), [StartUs](https://www.startus-insights.com/), [StartupBase](https://startupbase.io/)
* choose companies who are considered to be your competitors
* process the presented business information manually
* analyze the obtained results, formulate conclusions

This sounds time consuming, but it will be worth it. Maybe…

### **Help of a personal AI-driven product manager**

The Upfinity team provides an opportunity to validate tech-related business hypothesis in the real market, construct the idea, understand the competitive environment, analyze product features and monetization models.

The journey into the IT world begins at the Idea Soup. Taking a short expertise survey (Fig. 1) will help the user in picking relevant market categories.

![Figure 1. Upfinity interface example – «Survey»](../../../../img/выбор-предпочтений.png "Figure 1. Upfinity interface example – «Survey»")

Next, you can outline your ideal market environment by indication the value of several preselected market parameters on the Preference map (Fig. 2). After that, Upfinity’s AIS will recommend perfectly matching industries and help to compile a good list of rivals, plus construct a competitive idea.

![Figure 2. Upfinity interface example – «Preference map»](../../../../img/снимок-экрана-2021-04-20-в-00.28.34.png "Figure 2. Upfinity interface example – «Preference map»")

If survey answers do not correspond to the user's market preferences, a warning pop-up window will appear. For example, if the level of financial entry barriers does not match user's funding opportunities, the system will detect irrelevance and suggest lowering the feature’s value (Fig. 3).

![Figure 3. Upfinity interface example – Pop-up](../../../../img/предупреждение.png "Figure 3. Upfinity interface example – Pop-up")

## **Useful tips**

After completion of the research, an important decision on whether to implement the project has to be made. Sometimes after the idea validation, founders switch the focus to something fundamentally new.

Even if the hypothesis was not confirmed, another one can be assessed – automation allows to test any ideas unlimited number of times. As soon as it’s approved, you can proceed to build an implementation strategy. Use hypothesis tests to learn, [Upfinity’s](https://upfinity.io/?utm_source=organic&utm_medium=blog&utm_campaign=article1) virtual product manager will guide you all the way through. We believe in you!
import React from 'react';

import Hero2 from '../assets/svg/bird_1.svg'

// import Spark1 from '../assets/svg/spark_1.svg'
// import Spark2 from '../assets/svg/spark_2.svg'
// import Spark3 from '../assets/svg/spark_3.svg'
// import Spark4 from '../assets/svg/spark_4.svg'
// import Spark5 from '../assets/svg/spark_5.svg'
// import Spark6 from '../assets/svg/spark_6.svg'
// import Spark7 from '../assets/svg/spark_7.svg'
// import Spark8 from '../assets/svg/spark_8.svg'
// import Spark9 from '../assets/svg/spark_9.svg'

class Detecter extends React.Component {
    constructor(props) {
        super(props)

        this.state = {
            y: 0,
            scaleFactor: 1,
            opacity: 0.2,
            opacityBird: 1
        }

        this.container = React.createRef()
        this.handler = this.handler.bind(this)
    }

    componentDidMount() {
        window.addEventListener('scroll', (e) => this.handler(e));
    }

    handler() {
        if(this.container !== undefined && this.container.current != null) {
            this.setState({
                y: window.pageYOffset,
                scaleFactor: 1 + window.pageYOffset / 500,
                opacity: 0.2 + window.pageYOffset / 500,
                opacityBird: 1 - window.pageYOffset / 250
            })
        }
    }

    componentWillUnmount() {
        window.removeEventListener('scroll', this.handler)
    }

    render() {
        return (
            <React.Fragment>
                <div ref={this.container} className="bird_wrapper" style={{
                    transform: `translateY(-${this.state.y / 10}%) scale(${this.state.scaleFactor})`,
                    opacity: this.state.opacity
                }}>
                    <div className="spark_container hide-sm" style={{
                        opacity: this.state.opacityBird
                    }}>
                        {/* <Spark1 className="spark spark_1" />
                        <Spark2 className="spark spark_2" />
                        <Spark3 className="spark spark_3" />
                        <Spark4 className="spark spark_2" />
                        <Spark5 className="spark spark_5" />
                        <Spark6 className="spark spark_6" />
                        <Spark7 className="spark spark_7" />
                        <Spark8 className="spark spark_8" />
                        <Spark9 className="spark spark_9" /> */}
                    </div>

                    {/* <img src="../../bird_1.png" alt=""/> */}
                    
                    <Hero2 className="hero_2" />
                </div>
            </React.Fragment>
        )
    }
}

export default Detecter
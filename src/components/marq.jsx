import React from "react"
import { FormattedHTMLMessage, useIntl } from 'gatsby-plugin-intl';
import MarqRow from '../components/MarqRow'
import Modal from 'react-modal';
import axios from 'axios';
import * as EmailValidator from 'email-validator';
import { connect } from 'react-redux';
import { useAuth0 } from '@auth0/auth0-react';

import { useStaticQuery, graphql } from "gatsby"

Modal.setAppElement('#___gatsby');

const modalStyles = {
  overlay: {
    backgroundColor: "rgba(0, 0, 0, 0.58)",
  },
  content: {
    position: "relative",
    top: "auto",
    left: "auto",
    right: "auto",
    bottom: "auto",
    maxWidth: "560px",
    margin: "32px auto",
    padding: 0,
    border: 0,
    zIndex: 999,
    agree_box: false
  },
};

class Marq extends React.Component {
    constructor(props) {
        super(props)

        this.state = {
            currentBubble: null,
            modalOpen: false,
            count: 0,
            isSending: false,
            first_name: '',
            email: ''
        }

        this.openModal = this.openModal.bind(this)
        this.closeModal = this.closeModal.bind(this)
        this.sendLog = this.sendLog.bind(this)
        this.onChange = this.onChange.bind(this)
        this.validatingForm = this.validatingForm.bind(this)
        this.renderError = this.renderError.bind(this)
    }

    onChange(e) {
        if(e.target.type == 'checkbox') {
            this.setState({
              [e.target.name]: e.target.checked
            }, () => {
                this.validatingForm()
            })
        } else {
            this.setState({
              [e.target.name]: e.target.value
            }, () => {
                this.validatingForm()
            })
        }
      }

    sendLog(id) {
        if(!this.state.agree_box === true) {
            return false;
        }
        if(!EmailValidator.validate(this.state.email)) {
            return false;
        }
        if(this.state.first_name.trim().length > 1) {
            this.setState({isSending: true})
            axios.post(this.props.data.site.siteMetadata.endpoints.log + this.props.token, {
                type_id: id,
                data: {
                  button_clicked: 'industry',
                  email: this.state.email,
                  first_name: this.state.first_name,
                  term: this.state.term
                },
                timestamp: new Date().toISOString(),
                lang: this.props.intl.locale
              }, {
                headers: {
                  "Content-Type": "application/json",
                  "Accept": "application/json",
                }
              })
                .then(response => {
                    this.closeModal()
                    this.setState({isSending: false})
                    window.location = 'https://app.upfinity.io/';
                })
                .catch(e => {
                    console.log(e)
                    this.setState({isSending: false})
                })
                .then(() => {
                    this.closeModal()
                    this.setState({isSending: false})
                })
        }
    }

    numberWithSpaces = (x) => {
        return x.count.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ");
    }

    openModal(bubble) {
        this.setState({
            currentBubble: {
                ...bubble,
                count: this.numberWithSpaces(bubble)
            },
            modalOpen: true
        }, () => {
            this.validatingForm()
        })
      }
    
      closeModal() {
        this.setState({modalOpen: false})
      };
    
    getRandomInt = (min, max) => {
        return Math.random() * (max - min) + min;
    }

    renderRows() {
        let subarray = [], size = 5
        for (let i = 0; i < this.props.rows; i++){
            subarray[i] = this.props.items.slice((i*size), (i*size) + size);
        }

        return (
            <div>
                {Array.from({length: this.props.rows}).map((_, idx) => {
                    return (
                        <MarqRow
                            key={`row__${idx}`}
                            items={subarray[idx]}
                            speed={this.getRandomInt(0.05,1)}
                            clicked={(item) => this.openModal(item)}
                        ></MarqRow>
                    )
                })}
            </div>
        )
    }

    validatingForm() {
        if(this.state.first_name.trim().length < 2) {
            this.setState({
                error: {
                    title: 'first_name',
                    string: this.props.intl.formatMessage({ id: "validation.first_name" })
                },
                isSending: true
            })
            return false;
        }
        if(!EmailValidator.validate(this.state.email)) {
            this.setState({
                error: {
                    title: 'email',
                    string: this.props.intl.formatMessage({ id: "validation.email" })
                },
                isSending: true
            })
            return false;
        }
        if(!this.state.agree_box === true) {
            this.setState({
                error: {
                    title: 'agree_box',
                    string: this.props.intl.formatMessage({ id: "validation.agree" })
                },
                isSending: true
            })
            return false;
        }

        this.setState({
            error: null,
            isSending: false
        })
    }

    renderError(title) {
        if (this.state.error && this.state.error.title === title) {
            return <small>{ this.state.error.string }</small>
        } else {
            return null
        }
    }

    componentDidUpdate(prevProps) {
        if(JSON.stringify(this.props.auth) !== JSON.stringify(prevProps.auth)) {
            if(this.props.auth && this.props.auth.user && this.props.auth.user.email) {
                this.setState({
                    email: this.props.auth.user.email
                })
            }
    
            if(this.props.auth && this.props.auth.user && this.props.auth.user.given_name) {
                this.setState({
                    first_name: this.props.auth.user.given_name
                })
            }
        }
    }

    componentDidMount() {
        if(this.props.auth && this.props.auth.user && this.props.auth.user.email) {
            this.setState({
                email: this.props.auth.user.email
            })
        }

        if(this.props.auth && this.props.auth.user && this.props.auth.user.given_name) {
            this.setState({
                first_name: this.props.auth.user.given_name
            })
        }
    }

    render() {
        return (
            <div id="marq">
                {this.renderRows()}

                <Modal
                    isOpen={this.state.modalOpen}
                    onRequestClose={this.closeModal}
                    style={modalStyles}
                    contentLabel="Modal"
                    closeTimeoutMS={500}
                >
                {
                    this.state.currentBubble != null ? (
                    <div className="modal__body">
                        <button className="model__close" onClick={this.closeModal}><i className="icon icon-cross"></i></button>
                        <div className="modal__title">
                            <div className="h2 h1-bubble">
                            <FormattedHTMLMessage
                                id="home.itemsAble"
                                values={{cs: this.state.currentBubble.count, ts: this.state.currentBubble.label}}
                            />
                            </div>

                            <div className="form-group mt-5">
                                <label className="form-label" htmlFor="input-example-4"><FormattedHTMLMessage id="modal.name" /></label>
                                <input className="form-input" value={this.state.first_name} onChange={this.onChange} name="first_name" type="text" id="input-example-4" placeholder={this.props.intl.formatMessage({id: `modal.name`})} />
                                {this.renderError('first_name')}
                            </div>
                            <div className="form-group mb-5">
                                <label className="form-label" htmlFor="input-example-4">Email</label>
                                <input className="form-input" value={this.state.email} onChange={this.onChange} name="email" type="text" id="input-example-4" placeholder="Email" />
                                {this.renderError('email')}
                            </div>

                            <div>
                                <label className="form-radio mr-5 label-static">
                                <input type="checkbox" checked={this.state.agree_box === true} value={this.state.agree_box === true} name="agree_box" onChange={this.onChange} />
                                <i className="form-icon"></i> <FormattedHTMLMessage id="agree" /> <u><a href="/terms" target="_blank"><FormattedHTMLMessage id="terms" /></a></u></label>
                                {this.renderError('agree_box')}
                            </div>

                            <div>
                                <button disabled={this.state.isSending} onClick={() => this.sendLog(6)} className="btn btn-primary mt-12 px-6"><FormattedHTMLMessage id="free_of_charge" /></button>
                            </div>
                        </div>
                    </div>
                    ) : ''
                }
                </Modal>
            </div>
        )
    }
}

const SuperMarq = (props) => {
    const intl = useIntl();
    const auth = useAuth0();

    const data = useStaticQuery(graphql`
        query {
            site {
                siteMetadata {
                    endpoints {
                        signin
                        signup
                        purchase
                        subscribe
                        enterprise
                        soup
                        log
                    }
                }
            }
        }
    `)

    props = {
        ...props,
        data
    }

    return (
        <Marq intl={intl} {...props} auth={auth} />
    )
}

const mapStateToProps = ({ token }) => {
    return { token }
  }
  
  const mapDispatchToProps = dispatch => {
    return {}
  }

export default connect(mapStateToProps, mapDispatchToProps)(SuperMarq)
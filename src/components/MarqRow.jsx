import React from "react"
import { useIntl } from 'gatsby-plugin-intl';

class MarqRow extends React.Component {
    constructor(props) {
        super(props)

        this.state = {
            speed: 4,
            timer: null,
            progress: 1,
            isPositive: true,
            offset: 1
        }

        this.mounted = false
        this.oldScroll = 0

        this.container = React.createRef()
        this.inner = React.createRef()

        this.animater = this.animater.bind(this)
        this.handler = this.handler.bind(this)
    }

    checkVisible(elm) {
        let rect = elm.getBoundingClientRect();
        let viewHeight = Math.max(document.documentElement.clientHeight, window.innerHeight);
        return !(rect.bottom < 0 || rect.top - viewHeight >= 0);
    }

    componentDidMount() {
        this.mounted = true
        this.containerWidth = this.inner.current.offsetWidth

        window.requestAnimationFrame(this.animater)

        window.addEventListener('scroll', this.handler);
    }

    componentWillUnmount() {
        window.removeEventListener('scroll', this.handler)
        this.mounted = false
    }

    handler(e) {
        let that = this
        if(that.mounted) {
            if(that.container && that.checkVisible(that.container.current)) {
                if(that.oldScroll > window.pageYOffset) {
                    that.setState({isPositive: true})
                } else {
                    that.setState({isPositive: false})
                }

                that.setState({
                    offset: 7
                }, () => {
                    clearTimeout(that.state.timer)
                    that.setState({
                        timer: setTimeout(() => that.setState({
                                    offset: 1
                                }), 10)
                    })
                })

                that.oldScroll = window.pageYOffset
            }
        }
    }

    animater() {
        let reqId;

        if(!this.state.isPositive) {
            this.setState({
                progress: this.state.progress - this.props.speed * this.state.offset
            })
            
            
            if(this.state.progress <= this.containerWidth * -1) {
                this.setState({
                    progress: 0
                })
            }
        } else {
            this.setState({
                progress: this.state.progress + this.props.speed * this.state.offset
            })
            
            
            if(this.state.progress >= this.containerWidth) {
                this.setState({
                    progress: 0
                })
            }
        }
        
        if(this.mounted) {
            this.container.current.style.transform = 'translateX(' + this.state.progress + 'px)';

            window.setTimeout(() => {
                reqId = window.requestAnimationFrame(this.animater)
            }, 250 / 60)
        } else {
            window.cancelAnimationFrame(reqId);
        }
    }

    render() {
        return (
            <React.Fragment>
                <div ref={this.container} className="marq-row">
                    <div className="marq-inner">
                        {
                            this.props.items.map((item, index) => {
                                return (
                                    <span key={`item_1_${index}`} onClick={() => this.props.clicked(item)} className="marq-item">{item.label}</span>
                                )
                            })
                        }
                    </div>

                    <div ref={this.inner} className="marq-inner">
                        {
                            this.props.items.map((item, index) => {
                                return (
                                    <span key={`item_2_${index}`} onClick={() => this.props.clicked(item)} className="marq-item">{item.label}</span>
                                )
                            })
                        }
                    </div>

                    <div className="marq-inner">
                        {
                            this.props.items.map((item, index) => {
                                return (
                                    <span key={`item_3_${index}`} onClick={() => this.props.clicked(item)} className="marq-item">{item.label}</span>
                                )
                            })
                        }
                    </div>
                </div>
            </React.Fragment>
        )
    }
}

const SuperMarqRow = (props) => {
    const intl = useIntl()

    return (
        <MarqRow intl={intl} {...props} />
    )
}

export default SuperMarqRow;
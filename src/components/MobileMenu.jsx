import React from 'react';

class Menu extends React.Component {
    constructor(props) {
        super(props)

        this.state = {
            check: false
        }

        this.toggleCheck = this.toggleCheck.bind(this)
    }

    toggleCheck() {
        this.setState({
            check: !this.state.check
        })
    }

    render() {
        return (
            <React.Fragment>
                <div className={"mobile_menu " + (this.state.check ? 'menu-opened' : '')}>
                    <div>
                        {this.props.children}
                    </div>
                </div>

                <input type="checkbox" checked={this.state.check} onClick={this.toggleCheck} onChange={this.toggleCheck} id="menuToggle" />
                <label htmlFor="menuToggle"></label>
                <span className="spanner spanner-1"></span>
                <span className="spanner spanner-2"></span>
                <span className="spanner spanner-3"></span>
            </React.Fragment>
        )
    }
}

export default Menu
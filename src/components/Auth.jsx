import React from 'react';
import { useAuth0 } from '@auth0/auth0-react';

// import { Login, Logout, isAuthenticated, getProfile } from "../utils/auth";

// import { withAuth0 } from '@auth0/auth0-react';

const Auth = (props) => {
    const { user, isLoading, isAuthenticated, loginWithPopup, logout } = useAuth0();

    if(isLoading) {
        return (
            <div className="user-holder">...</div>
        )
    }
    
    const goCommunity = () => {
        window.location = process.env.GATSBY_COMMUNITY_LINK;
    }

    const goPreferences = () => {
        window.location = process.env.GATSBY_PREFERENCES_LINK;
    }

    return (
        <>
        {
            isAuthenticated ? (
                <>
                    <button className="btn btn-outline-primary px-6 font-bold ml-12" onClick={() => goCommunity()}>{ props.intl.formatMessage({ id: "community" }) }</button>
                    <button className="btn btn-primary font-bold px-6 mx-4" onClick={() => goPreferences()}>{ props.intl.formatMessage({ id: "start-right-now" }) }</button>
                    
                    <div className="dropdown dropdown-right line-1-0">
                        <div className="dropdown-toggle user-holder line-1-0"><span>{ user.name }</span></div>
                        <ul className="menu menu-right">
                            <li className="menu-item">
                                <button className="btn btn-outline-transparent w-100 text-left" onClick={() => logout()}>{ props.intl.formatMessage({ id: "logout" }) }</button>
                            </li>
                        </ul>
                    </div>
               </>
            ) : (
                <>
                    <button className="btn btn-outline-primary px-6 font-bold ml-12" onClick={() => loginWithPopup()}>{ props.intl.formatMessage({ id: "sign-in" }) }</button>
                    <button className="btn btn-primary font-bold px-6 mx-4" onClick={() => goPreferences()}>{ props.intl.formatMessage({ id: "start-now" }) }</button>
                </>
            )

        }

        
        </>
    )
}

export default Auth;
import React from "react"
import { Link, FormattedHTMLMessage, useIntl } from 'gatsby-plugin-intl';
import Modal from 'react-modal';
import axios from 'axios';
import * as EmailValidator from 'email-validator';
import { connect } from 'react-redux'

import { useStaticQuery, graphql } from "gatsby"

Modal.setAppElement('#___gatsby');

const modalStyles = {
  overlay: {
    backgroundColor: "rgba(0, 0, 0, 0.58)",
  },
  content: {
    position: "relative",
    top: "auto",
    left: "auto",
    right: "auto",
    bottom: "auto",
    maxWidth: "560px",
    margin: "32px auto",
    padding: 0,
    border: 0,
    zIndex: 999,
    agree_box: false
  },
};

class Footer extends React.Component {
    constructor(props) {
        super(props)

        this.state = {
            subscribe: '',
            isSubscribed: false,
            modalOpen: false,
            first_name: '',
            email: '',
            agree_box: false,
            isSending: false,
            error: null
        }

        this.openModalSubscribe = this.openModalSubscribe.bind(this)
        this.closeModalSubscribe = this.closeModalSubscribe.bind(this)
        this.subscribe = this.subscribe.bind(this)
        this.onChange = this.onChange.bind(this)
        this.validatingForm = this.validatingForm.bind(this)
        this.renderError = this.renderError.bind(this)
    }

    validatingForm() {
        if(this.state.first_name.trim().length < 2) {
            this.setState({
                error: {
                    title: 'first_name',
                    string: this.props.intl.formatMessage({ id: "validation.first_name" })
                },
                isSending: true
            })
            return false;
        }
        if(!EmailValidator.validate(this.state.email)) {
            this.setState({
                error: {
                    title: 'email',
                    string: this.props.intl.formatMessage({ id: "validation.email" })
                },
                isSending: true
            })
            return false;
        }
        if(!this.state.agree_box === true) {
            this.setState({
                error: {
                    title: 'agree_box',
                    string: this.props.intl.formatMessage({ id: "validation.agree" })
                },
                isSending: true
            })
            return false;
        }

        this.setState({
            error: null,
            isSending: false
        })
    }

    closeModalSubscribe() {
        this.setState({modalOpen: false})
    }

    onChange(e) {
        if(e.target.type == 'checkbox') {
            this.setState({
                [e.target.name]: e.target.checked
            }, () => {
                this.validatingForm()
            })
        } else {
            this.setState({
                [e.target.name]: e.target.value
            }, () => {
                this.validatingForm()
            })
        }
    }

    openModalSubscribe() {
        this.setState({
            modalOpen: true
        }, () => {
            this.validatingForm()
        })
    }

    subscribe() {
        if(!this.state.agree_box === true) {
            return false;
        }
        if(!EmailValidator.validate(this.state.email)) {
            return false;
        }
        if(this.state.first_name.trim().length > 1) {
            this.setState({isSending: true})
            axios.post(this.props.data.site.siteMetadata.endpoints.log + this.props.token, {
                type_id: 12,
                data: {
                    button_clicked: 'footer_subscribe',
                    first_name: this.state.first_name,
                    email: this.state.email
                },
                timestamp: new Date().toISOString(),
                lang: this.props.intl.locale
                }, {
                headers: {
                    "Content-Type": "application/json",
                    "Accept": "application/json",
                }
                })
                .then(response => {
                    this.closeModalSubscribe()
                    this.setState({isSending: false})
                })
                .catch(e => {
                    console.log(e)
                    this.setState({isSending: false})
                })
                .then(() => {
                    this.closeModalSubscribe()
                    this.setState({isSending: false})
                })
        }
    }

    renderError(title) {
        if (this.state.error && this.state.error.title === title) {
            return <small>{ this.state.error.string }</small>
        } else {
            return null
        }
    }

    render() {
        return (
            <footer className="footer">
                <div className="footer-upper">
                    <div className="container">
                        <div className="h1">Enjoy being a founder!</div>
                    
                        <p className="font-half">{ this.props.intl.formatMessage({ id: "being_founder_body" }) }</p>
                    </div>
                </div>
                <div className="footer-lower">
                    <div className="container">
                        <div className="columns">
                            <div className="column col-4 col-md-12 col-sm-12">
                                <h4 className="text-uppercase">{ this.props.intl.formatMessage({ id: "about_us" }) }</h4>
    
                                <div className="text-muted">{ this.props.intl.formatMessage({ id: "about_body" }) }</div>

                                <div className="my-6">
                                    <a href="https://www.ccifr.ru/ward/400_member_companies?formcompany=upfinity&formcategory=0&formsector=0" target="_blank">
                                        <img src="/frtpp.svg" alt="frtpp" style={{maxWidth: '250px' }} />
                                    </a>

                                    <div className="mt-3">
                                        <small className="text-muted d-inline-block line-1">{ this.props.intl.formatMessage({ id: "frtpp" }) }</small>
                                    </div>
                                </div>
                            </div>
                            <div className="column col-1 hide-md"></div>
                            <div className="column col-7 col-md-12 col-sm-12 mt-12-mobile">
                                <h4 className="text-uppercase">{ this.props.intl.formatMessage({ id: "navigation" }) }</h4>
    
                                <div className="navbar">
                                    <section className="navbar-section flex-wrap-wrap">
                                        <Link to="/company" className="btn btn-link-ligth font-bold mr-4 pl-0">{ this.props.intl.formatMessage({ id: "company" }) }</Link>
                                        <Link to="/team" className="btn btn-link-ligth font-bold mr-4 pl-0">{ this.props.intl.formatMessage({ id: "team" }) }</Link>
                                        <Link to="/plans" className="btn btn-link-ligth font-bold mr-4 pl-0">{ this.props.intl.formatMessage({ id: "plans" }) }</Link>
                                        <Link to="/solutions" className="btn btn-link-ligth font-bold mr-4 pl-0">{ this.props.intl.formatMessage({ id: "solutions" }) }</Link>
                                        <Link to="/blog" className="btn btn-link-ligth font-bold mr-4 pl-0">{ this.props.intl.formatMessage({ id: "blog" }) }</Link>
                                        <Link to="/terms" className="btn btn-link-ligth font-bold mr-4 pl-0">{ this.props.intl.formatMessage({ id: "terms" }) }</Link>
                                        <Link to="/privacy" className="btn btn-link-ligth font-bold mr-4 pl-0">{ this.props.intl.formatMessage({ id: "policy" }) }</Link>
                                    </section>
                                </div>
                            </div>
                            <div className="column col-12 col-md-12 mt-6"></div>
    
                            <div className="column col-4 col-md-12 col-sm-12">
                                <h4>{ this.props.intl.formatMessage({ id: "contacts" }) }</h4>
    
                                <div className="mt-6">
                                    
                                </div>

                                <a href={`mailto:${this.props.data.site.siteMetadata.endpoints[this.props.intl.locale].email}`}><img className="locale_icon mr-4" src="/email_icon.png" alt="Upfinity email"/></a>
                                <a rel="noopener" rel="noreferrer" target="_blank" href={this.props.data.site.siteMetadata.endpoints[this.props.intl.locale].facebook}><img className="locale_icon mr-4" src="/fb_icon.png" alt="Upfinity facebook"/></a>
                                <a rel="noopener" rel="noreferrer" target="_blank" href={this.props.data.site.siteMetadata.endpoints[this.props.intl.locale].linkedin}><img className="locale_icon" src="/linked_icon.png" alt="Upfinity linkedin"/></a>
                            </div>
                            <div className="column col-1"></div>
                            <div className="column col-7 col-md-12">
                                <h4>{ this.props.intl.formatMessage({ id: "news" }) }</h4>
    
                                <div className="columns">
                                    <div className="column col-7 col-md-12">
                                        {
                                            !this.state.isSubscribed ? (
                                                <div>
                                                    <button onClick={() => this.openModalSubscribe()} className="btn btn-primary input-group-btn">{ this.props.intl.formatMessage({ id: "submit_news" }) }</button>
                                                </div>
                                            ) : (
                                                <div>{ this.props.intl.formatMessage({ id: "submission" }) }</div>
                                            )
                                        }
                                    </div>
                                </div>
    
                                <div className="mt-12 text-right text-center-mobile text-muted">© 2021 Upfinity, LLC | { this.props.intl.formatMessage({ id: "made" }) }</div>

                                <div className="text-right text-center-mobile">
                                    <small>
                                        <a href="#" className="text-muted no-link">ОГРНИП 321774600061797 | ИНН 540822801796</a>
                                    </small>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <Modal
                    isOpen={this.state.modalOpen}
                    onRequestClose={this.closeModalSubscribe}
                    style={modalStyles}
                    contentLabel="Modal"
                    closeTimeoutMS={500}
                >
                    <div className="modal__body">
                        <button className="model__close" onClick={this.closeModalSubscribe}><i class="icon icon-cross"></i></button>
                        <div className="modal__name"><FormattedHTMLMessage id="submit_title" /></div>

                        <div className="form-group mt-5">
                            <label className="form-label" htmlFor="input-example-4"><FormattedHTMLMessage id="modal.name" /></label>
                            <input className="form-input" value={this.state.first_name} onChange={this.onChange} name="first_name" type="text" id="input-example-4" placeholder={this.props.intl.formatMessage({id: `modal.name`})} />
                            {this.renderError('first_name')}
                        </div>

                        <div className="form-group mb-5">
                            <label className="form-label" htmlFor="input-example-4">Email</label>
                            <input className="form-input" value={this.state.email} onChange={this.onChange} name="email" type="text" id="input-example-4" placeholder="Email" />
                            {this.renderError('email')}
                        </div>

                        <div>
                            <label className="form-radio mr-5 label-static">
                            <input type="checkbox" checked={this.state.agree_box === true} value={this.state.agree_box === true} name="agree_box" onChange={this.onChange} />
                            <i className="form-icon"></i> <FormattedHTMLMessage id="agree" /> <u><a href="/terms" target="_blank"><FormattedHTMLMessage id="terms" /></a></u></label>

                            {this.renderError('agree_box')}
                        </div>

                        <button disabled={this.state.isSending} className="btn btn-primary font-bold px-6 my-5" onClick={() => this.subscribe()}><FormattedHTMLMessage id="submit_cta" /></button>
                    </div>
                </Modal>
            </footer>
        )
    }
}

const SuperFooter = (props) => {
    const intl = useIntl()

    const data = useStaticQuery(graphql`
        query {
            site {
                siteMetadata {
                    endpoints {
                        formcarry
                        en {
                            email
                            linkedin
                            facebook
                        }
                        ru {
                            email
                            linkedin
                            facebook
                        }
                        log
                    }
                }
            }
        }
    `)

    props = {
        ...props,
        data
    }

    return (
        <Footer intl={intl} {...props} />
    )
}

const mapStateToProps = ({ token }) => {
    return { token }
}
  
const mapDispatchToProps = dispatch => {
    return {}
}
  
export default connect(mapStateToProps, mapDispatchToProps)(SuperFooter)
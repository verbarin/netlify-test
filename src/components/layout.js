import React from "react"

import Header from '../components/Header'
import Footer from '../components/Footer'

const Layout = ({ location, title, children }) => {
  return (
    <React.Fragment>
      <Header />
      <main>{children}</main>
      <Footer />
    </React.Fragment>
  )
}

export default Layout

import React, { Component } from "react"
import { Link, changeLocale, useIntl } from 'gatsby-plugin-intl';
import Logo from '../assets/svg/logo.svg';
import Media from 'react-media';
import MobileMenu from './MobileMenu';
import CookieConsent from "react-cookie-consent";

import { useStaticQuery, graphql } from "gatsby"
import axios from "axios";
import Auth from './Auth';

import { connect } from 'react-redux';

class Header extends Component {
    constructor(props) {
        super(props)
    }

    componentDidMount() {
        window.addEventListener('scroll', function() {
            const navbar = document.getElementById('navbar')

            if(navbar) {
                if(window.pageYOffset > 50) {
                    navbar.classList.add('shadow-sm')
                    navbar.classList.remove('py-6')
                    navbar.classList.add('py-1')
                } else {
                    navbar.classList.remove('shadow-sm')
                    navbar.classList.remove('py-1')
                    navbar.classList.add('py-6')
                }
            }
        });

        this.handleToken()
    }

    handleToken() {
        let cookieToken = this.getCookieValue('basic-initial-token')
        if(cookieToken.trim() != '') {
            this.props.setToken(cookieToken)
        } else {
            const urlParams = new URLSearchParams(window.location.search);
            const utms = {};

            urlParams.get('utm_source') != null ? utms.utm_source = urlParams.get('utm_source') : ''
            urlParams.get('utm_medium') != null ? utms.utm_medium = urlParams.get('utm_medium') : ''
            urlParams.get('utm_campaign') != null ? utms.utm_campaign = urlParams.get('utm_campaign') : ''
            urlParams.get('utm_term') != null ? utms.utm_term = urlParams.get('utm_term') : ''
            urlParams.get('utm_content') != null ? utms.utm_content = urlParams.get('utm_content') : ''

            axios.post('https://upfinity-proxy.herokuapp.com/cl/tokens/', {
                lang: this.props.intl.locale,
                data: Object.keys(utms).length === 0 ? {} : {
                    utm: utms
                }
            })
                .then(response => {
                    if(response.data.success) {
                        document.cookie = "basic-initial-token=" + response.data.result;
                        this.props.setToken(response.data.result)
                    }
                })
                .catch(e => console.log(e))
        }

        
    }

    getCookieValue = (name) => (
        document.cookie.match('(^|;)\\s*' + name + '\\s*=\\s*([^;]+)')?.pop() || ''
      )

    renderLocale() {
        let component = null;
        
        switch(this.props.intl.locale) {
            case 'en':
                component = 'English (US)'
                break;
            case 'ru':
                component = 'Русский'
                break;
            default:
                component = null
        }
        return (
            component
        )
    }

    changeLocaleAndRefresh = (locale) =>{
        changeLocale(locale);
    }

    render() {
        return (
            <div>
                <span className="display-1122">
                    <React.Fragment>
                        <MobileMenu>
                            <Link to="/">
                                <Logo />
                            </Link>

                            <ul className="mt-12">
                                <li className="mobile-link-1"><Link to="/company">{ this.props.intl.formatMessage({ id: "company" }) }</Link></li>
                                <li className="mobile-link-2"><Link to="/team">{ this.props.intl.formatMessage({ id: "team" }) }</Link></li>
                                <li className="mobile-link-3"><Link to="/plans">{ this.props.intl.formatMessage({ id: "plans" }) }</Link></li>
                                <li className="mobile-link-4"><Link to="/solutions">{ this.props.intl.formatMessage({ id: "solutions" }) }</Link></li>
                                <li className="mobile-link-5"><Link to="/blog">{ this.props.intl.formatMessage({ id: "blog" }) }</Link></li>
                            </ul>

                            <div className="mt-12">
                                <button className="btn btn-outline-black mr-3" onClick={() => changeLocale('en')} tabIndex="-1"><img src="/../../globe.svg" className="locale_icon" alt=""/> English</button>
                                <button className="btn btn-outline-black" onClick={() => changeLocale('ru')} tabIndex="-1"><img src="/../../globe.svg" className="locale_icon" alt=""/> Русский</button>
                            </div>
                        </MobileMenu>
                        <div className="text-center my-2">
                            <Link to="/">
                                <Logo />
                            </Link>
                        </div>
                    </React.Fragment>
                </span>
                <span className="hide-1122">
                    <header className="py-6" id="navbar">
                        <div className="container navbar">
                            <section className="navbar-section">
                                <Link to="/" className="navbar-brand mr-12">
                                    <Logo />
                                </Link>
                                <div className="dropdown mx-1">
                                    <div className="btn btn-link dropdown-toggle font-bold">
                                    { this.props.intl.formatMessage({ id: "about" }) }
                                    </div>
                                    <ul className="menu">
                                        <li className="menu-item"><Link to="/company">{ this.props.intl.formatMessage({ id: "company" }) }</Link></li>
                                        <li className="menu-item"><Link to="/team">{ this.props.intl.formatMessage({ id: "team" }) }</Link></li>
                                    </ul>
                                </div>
                                <Link to="/plans" className="btn btn-link font-bold mx-1">{ this.props.intl.formatMessage({ id: "plans" }) }</Link>
                                <Link to="/solutions" className="btn btn-link font-bold mx-1">{ this.props.intl.formatMessage({ id: "solutions" }) }</Link>
                                <Link to="/blog" className="btn btn-link font-bold mx-1">{ this.props.intl.formatMessage({ id: "blog" }) }</Link>
                                
                            </section>
                            <section className="navbar-section">
                                <Auth intl={this.props.intl} {...this.props} />
                                <div className="dropdown dropdown-right mx-4">
                                    <div className="btn btn-link dropdown-toggle font-bold">
                                    <img src="/../../globe.svg" className="locale_icon" alt=""/>
                                    </div>
                                    <ul className="menu menu-right">
                                        <li className="menu-item"><button className="btn btn-outline-transparent w-100 text-left" onClick={() => this.changeLocaleAndRefresh('en')} tabIndex="-1">English</button></li>
                                        <li className="menu-item"><button className="btn btn-outline-transparent w-100 text-left" onClick={() => this.changeLocaleAndRefresh('ru')} tabIndex="-1">Русский</button></li>
                                    </ul>
                                </div>
                                {/* <a href={this.props.data.site.siteMetadata.endpoints.signin} target="_blank" className="btn btn-outline-primary px-6 font-bold ml-12">{ this.props.intl.formatMessage({ id: "sign-in" }) }</a>
                                <a href={this.props.data.site.siteMetadata.endpoints.signup} target="_blank" className="btn btn-primary font-bold px-6 mx-4">{ this.props.intl.formatMessage({ id: "start-now" }) }</a> */}
                            </section>
                        </div>
                    </header>
                </span>

                <CookieConsent
                    location="bottom"
                    buttonText="OK"
                    cookieName="cookie-consent"
                    style={{ background: "#121212" }}
                    buttonStyle={{ background: "#fff", color: "#4e503b", fontSize: "13px" }}
                    expires={150}
                    >
                    { this.props.intl.formatMessage({ id: "cookie" }) }
                </CookieConsent>
            </div>
        )
    }
}

const SuperHeader = (props) => {
    const intl = useIntl()

    const data = useStaticQuery(graphql`
        query {
            site {
                siteMetadata {
                    endpoints {
                        signin
                        signup
                        purchase
                        subscribe
                        enterprise
                    }
                }
            }
        }
    `)

    props = {
        ...props,
        data
    }

    return (
        <Header intl={intl} {...props} />
    )
}

const mapStateToProps = ({ token }) => {
    return { token }
  }
  
  const mapDispatchToProps = dispatch => {
    return { setToken: (token) => dispatch({ type: `SET_TOKEN`, token: token }) }
  }

export default connect(mapStateToProps, mapDispatchToProps)(SuperHeader)
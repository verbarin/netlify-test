import React from 'react';

class Detecter extends React.Component {
    constructor(props) {
        super(props)

        this.state = {
            isAchieved: false
        }

        this.container = React.createRef()
        this.handler = this.handler.bind(this)
    }

    componentDidMount() {
        window.addEventListener('scroll', (e) => this.handler(e));
    }

    handler() {
        let that = this

        if(that.container !== undefined && that.container.current != null && !that.state.isAchieved) {
            if(that.checkVisible(that.container.current)) {
                that.props.detected()
                that.setState({
                    isAchieved: true
                })
            }
        }
    }

    componentWillUnmount() {
        window.removeEventListener('scroll', this.handler)
    }

    checkVisible(elm) {
        let rect = elm.getBoundingClientRect();
        let viewHeight = Math.max(document.documentElement.clientHeight, window.innerHeight);
        return !(rect.bottom < 0 || rect.top - viewHeight >= 0);
    }

    render() {
        return (
            <div ref={this.container}>
                {this.props.children}
            </div>
        )
    }
}

export default Detecter
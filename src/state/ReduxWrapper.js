import React from "react"
import { Provider } from "react-redux"
import { Auth0Provider } from '@auth0/auth0-react';

import createStore from "./app";

const onRedirectCallback = (appState) => {
  // Use Gatsby's navigate method to replace the url
  navigate(appState?.returnTo || '/', { replace: true });
};

// eslint-disable-next-line react/display-name,react/prop-types
export default ({ element }) => {
  // Instantiating store in `wrapRootElement` handler ensures:
  //  - there is fresh store for each SSR page
  //  - it will be called only once in browser, when React mounts
  const store = createStore()
  return <Auth0Provider
            domain={process.env.GATSBY_AUTH0_DOMAIN}
            clientId={process.env.GATSBY_AUTH0_CLIENTID}
            redirectUri={process.env.GATSBY_AUTH0_REDIRECT}
            onRedirectCallback={onRedirectCallback}
            response_type="code|token"
          >
    <Provider store={store}>
      {element}
    
    </Provider>
  </Auth0Provider>
}
import { createStore as reduxCreateStore } from "redux"

const SET_TOKEN = 'SET_TOKEN';

const initialState = {
    token: null,
};

// export const setToken = (token) => ({
//   type: SET_TOKEN,
//   token
// });

const reducer = (state = initialState, action) => {
    switch (action.type) {
      case SET_TOKEN:
        return { ...state, token: action.token };
      default:
        return state;
    }
};

const createStore = () => reduxCreateStore(reducer, initialState)
export default createStore
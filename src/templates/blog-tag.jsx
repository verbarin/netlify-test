import React from "react"
import Header from '../components/Header'
import Footer from '../components/Footer'
import { Link, useIntl } from 'gatsby-plugin-intl';
import SEO from "../components/seo";
import { graphql } from "gatsby";

const BlogPage = ({ data, pageContext }) => {
  const posts = data.allMarkdownRemark.nodes

  const intl = useIntl()

  if (posts.length === 0) {
    return (
      <React.Fragment>
        <Header />
        <main id="app">
          <div className="container">
            <h2 className="text-center my-12">Our insights and news</h2>
            <h1>#{intl.formatMessage({ id: pageContext.tags })}</h1>

            <p>No topics here right now...</p>
          </div>
        </main>
        <Footer />
      </React.Fragment>
    )
  }

  return (
    <React.Fragment>
      <SEO
        title={pageContext.tags}
        description={pageContext.tags}
        lang={intl.locale}
      />
      <Header />
      <main id="app">
        <div className="container">
          <h1 className="text-center my-12">#{pageContext.tags}</h1>

          <div className="columns mt-6">
            {
              posts.map(post => {
                return (
                  intl.locale === post.frontmatter.locale ? 
                  (
                    <div key={`article_${post.fields.slug}`} className="column col-4 col-sm-12 col-md-6 mb-4">
                      <article className="blog-card">
                        <div className="img-background" style={{backgroundImage: `url(${post.frontmatter.thumbnail})`}}></div>

                        <header><h3 className="h4">
                          <Link to={`/blog/article${post.fields.slug}`}>{post.frontmatter.title}</Link>
                          </h3></header>
                        

                        <section><p>{post.frontmatter.description}</p></section>

                        <div className="tags-wrapper">
                          {
                            post.frontmatter.tags ? (
                              post.frontmatter.tags.map(tag => {
                                return (
                                  <span>#{tag}</span>
                                )
                              })
                            ) : ('')
                          }
                        </div>
                      </article>
                    </div>
                  ) : ('')
                )
              })
            }

            <div className="column col-12 d-flex mt-6 justify-content-center">
              {
                pageContext.numPages > 1 ? (
                  <ul className="pagination">
                
                  {
                    pageContext.currentPage == 1 && (
                      <li className="page-item disabled">
                        <Link to={`/blog/page/${pageContext.currentPage - 1}`}>{ intl.formatMessage({ id: "previous" }) }</Link>
                      </li>
                    )
                  }

                  {
                    pageContext.currentPage == 2 && (
                      <li className="page-item">
                        <Link to={`/blog/tag/${pageContext.slug}`}>{ intl.formatMessage({ id: "previous" }) }</Link>
                      </li>
                    )
                  }

                  {
                    pageContext.currentPage != 1 && pageContext.currentPage != 2 && (
                      <li className="page-item">
                        <Link to={`/blog/tag/${pageContext.slug}/page/${pageContext.currentPage - 1}`}>{ intl.formatMessage({ id: "previous" }) }</Link>
                      </li>
                    )
                  }
                  
                  <li className="page-item">
                    <span>{ pageContext.currentPage } / { pageContext.numPages }</span>
                  </li>

                  {
                    pageContext.currentPage >= pageContext.numPages && (
                      <li className="page-item disabled">
                        <Link to={`/blog/tag/${pageContext.slug}/page/${pageContext.currentPage + 1}`}>{ intl.formatMessage({ id: "next" }) }</Link>
                      </li>
                    )
                  }

                  {
                    pageContext.currentPage < pageContext.numPages && (
                      <li className="page-item">
                        <Link to={`/blog/tag/${pageContext.slug}/page/${pageContext.currentPage + 1}`}>{ intl.formatMessage({ id: "next" }) }</Link>
                      </li>
                    )
                  }
                </ul>
                ) : null
              }
            </div>
            

            
          </div>
        </div>
      </main>
      <Footer />
    </React.Fragment>
  )
}

export const pageQuery = graphql`
  query blogPostsListByTag($tags: String, $skip: Int!, $limit: Int!) {
    site {
      siteMetadata {
        title
      }
    }
    allMarkdownRemark(
      filter: { frontmatter: { tags: { in: [$tags] } } }
      sort: { fields: [frontmatter___date], order: DESC }
      skip: $skip,
      limit: $limit
      ) {
      nodes {
        excerpt
        fields {
          slug
          locale
        }
        frontmatter {
          date
          title
          description
          locale
          thumbnail
          tags
        }
      }
    }
  }
`

export default BlogPage
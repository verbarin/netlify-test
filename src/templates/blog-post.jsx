import React from "react"
import { graphql } from "gatsby"

import { DateTime } from "luxon"
import { Link, useIntl } from "gatsby-plugin-intl"
import Layout from "../components/layout"
import SEO from "../components/seo"
import kebabCase from 'lodash.kebabcase'

const BlogPostTemplate = ({ data, location }) => {
  const intl = useIntl()
  const post = data.markdownRemark
  const siteTitle = data.site.siteMetadata?.title || `Title`
  const { previous, next } = data

  const dateObject = DateTime.fromISO(post.frontmatter.date).setLocale(intl.locale)
  const formattedTitleDate = dateObject.toLocaleString()
  const formattedDisplayingDate = dateObject.toLocaleString(DateTime.DATE_FULL)

  return (
    <Layout location={location} title={siteTitle}>
      <SEO
        title={post.frontmatter.title}
        description={post.frontmatter.description || post.excerpt}
        lang={intl.locale}
        date={post.frontmatter.date}
      />
      <div className="container">
        <div className="columns">
          <div className="col-12 col-md-12 padding-mobile">
          <article
            className="blog-post"
            itemScope
            itemType="http://schema.org/Article"
          >
            <header>
              {
                post.frontmatter.category && (
                  post.frontmatter.category.map(cat => {
                    return (
                      <span className="chip mt-12"><Link to={`/blog/category/${kebabCase(cat)}`}>{intl.formatMessage({ id: kebabCase(cat) })}</Link></span>
                    )
                  })
                )
              }

              <h1 itemProp="headline" className="blog-header mt-12">{post.frontmatter.title}</h1>
              <div className="mb-6 text-muted">
                <time dateTime={post.frontmatter.date} title={formattedTitleDate}>
                  {formattedDisplayingDate}
                </time>
              </div>

              {
                post.frontmatter.tags && (
                  post.frontmatter.tags.map((tag, index) => {
                    return (
                      <span key={`tag_${index}`} className="chip"><Link to={`/blog/tag/${kebabCase(tag)}`}>#{tag}</Link></span>
                    )
                  })
                )
              }
              {/* <p>In: {post.frontmatter.tags.join()}</p> */}
            </header>
            <section
              dangerouslySetInnerHTML={{ __html: post.html }}
              itemProp="articleBody"
              className="blog-article mt-12"
            />
          </article>
          </div>
        </div>
      </div>
      <div className="container px-0">
        <nav className="blog-post-nav">
          <ul
            style={{
              display: `flex`,
              flexWrap: `wrap`,
              justifyContent: `space-between`,
              listStyle: `none`,
              padding: 0,
              margin: 0
            }}
          >
            <li className="prev-link">
              {
                previous && previous.frontmatter.locale === intl.locale ? (
                  <Link to={`/blog/article${previous.fields.slug}`} rel="prev">
                    ← {previous.frontmatter.title}
                  </Link>) : (null)
              }
            </li>
            <li className="next-link">
              {
                next && next.frontmatter.locale === intl.locale ? (
                  <Link to={`/blog/article${next.fields.slug}`} rel="next">
                  {next.frontmatter.title} →
                </Link>) : (null)
              }
            </li>
          </ul>
        </nav>
      </div>
    </Layout>
  )
}

export default BlogPostTemplate

export const pageQuery = graphql`
  query BlogPostBySlug(
    $id: String!
    $previousPostId: String
    $nextPostId: String
  ) {
    site {
      siteMetadata {
        title
      }
    }
    markdownRemark(id: { eq: $id }) {
      id
      excerpt(pruneLength: 160)
      html
      frontmatter {
        title
        date
        description
        category
        tags
      }
    }
    previous: markdownRemark(id: { eq: $previousPostId }) {
      fields {
        slug
      }
      frontmatter {
        title
        locale
      }
    }
    next: markdownRemark(id: { eq: $nextPostId }) {
      fields {
        slug
      }
      frontmatter {
        title
        locale
      }
    }
  }
`

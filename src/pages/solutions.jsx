import React from "react"
import SEO from "../components/seo"
import { FormattedHTMLMessage, useIntl } from 'gatsby-plugin-intl';
import Header from '../components/Header'
import Footer from '../components/Footer'

class SolutionsPage extends React.Component {
  goPreferences = () => {
    window.location = process.env.GATSBY_PREFERENCES_LINK;
  }

  render() {
    return (
      <React.Fragment>
        <SEO
          title={this.props.intl.formatMessage({ id: "seo.solutions_title" })}
          description={this.props.intl.formatMessage({ id: "seo.solutions_description" })}
          lang={this.props.intl.locale}
        />
        <Header />
        <main id="app">
          <div className="container">
            <h1 className="text-center mt-12 mb-0"><FormattedHTMLMessage id="solutions_page.title" /></h1>
  
            <p className="text-center font-half mb-12"><FormattedHTMLMessage id="solutions_page.subtitle" /></p>

            <h2 className="text-center"><FormattedHTMLMessage id="solutions_page.works" /></h2>
  
            <div className="columns">
              <div className="column col-12">
                <div className="tile">
                  <div className="tile-icon">
                    <div className="tile-number">1</div>
                  </div>
                  <div className="tile-content">
                    <p className="tile-title"><FormattedHTMLMessage id="solutions_page.block_1_title" /></p>

                    <video ref={this.videoRef1} className="simple-video mb-12" src={`../../upf-landing-hiw-01-pref-${this.props.intl.locale}.mp4`} controls>
                      <track kind="captions" />
                    </video>
                  </div>
                </div>
              </div>

              <div className="column col-12 mt-5">
                <div className="tile">
                  <div className="tile-icon">
                    <div className="tile-number">2</div>
                  </div>
                  <div className="tile-content">
                    <p className="tile-title"><FormattedHTMLMessage id="solutions_page.block_2_title" /></p>
                    <video ref={this.videoRef2} className="simple-video mb-12" src="../../upf-landing-hiw-02-soup-v2.mp4" controls>
                      <track kind="captions" />
                    </video>
                  </div>
                </div>
              </div>

              <div className="column col-12 mt-5">
                <div className="tile">
                  <div className="tile-icon">
                    <div className="tile-number">3</div>
                  </div>
                  <div className="tile-content">
                    <p className="tile-title"><FormattedHTMLMessage id="solutions_page.block_6_title" /></p>

                    <video ref={this.videoRef6} className="simple-video mb-12" src={`../../upf-landing-hiw-06-roadahead-v2-${this.props.intl.locale}.mp4`} controls>
                      <track kind="captions" />
                    </video>
                  </div>
                </div>
              </div>

              <div className="column col-12 text-center mt-12">
                <div className="h1"><FormattedHTMLMessage id="home.block_precta" /></div>

                <button onClick={this.goPreferences} className="btn btn-xl btn-primary mt-12"><FormattedHTMLMessage id="home.block_cta" /></button>
              </div>
            </div>
        </div>
        </main>
        <Footer />
      </React.Fragment>
    )
  }
}

const SuperSolutionsPage = (props) => {
  const intl = useIntl();

  return (
    <SolutionsPage intl={intl} {...props} />
  )
}

export default SuperSolutionsPage
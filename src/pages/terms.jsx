import React from "react"
import Header from './../components/Header'
import Footer from './../components/Footer'
import SEO from "../components/seo"
import { FormattedHTMLMessage, useIntl } from "gatsby-plugin-intl"

const TermsPage = () => {
  const intl = useIntl()
  
  return (
    <React.Fragment>
      <SEO
        title="Terms"
      />
      <Header />
      <main id="app">
        <div className="container">
            <h1 className="text-center my-12">Terms of usage</h1>
            <div>
                <p>Under maintenance</p>
            </div>
        </div>
      </main>
      <Footer />
    </React.Fragment>
  )
}
export default TermsPage
import React from "react"

import Header from './../../components/Header'
import Footer from './../../components/Footer'
import SEO from "../../components/seo"
import { Link, FormattedHTMLMessage } from 'gatsby-plugin-intl';
import axios from "axios";
import Modal from 'react-modal';
import { connect } from 'react-redux'
import * as EmailValidator from 'email-validator';
import { graphql } from "gatsby";
import { useAuth0 } from '@auth0/auth0-react';

import { useIntl } from "gatsby-plugin-intl";

Modal.setAppElement('#___gatsby');

const modalStyles = {
  overlay: {
    backgroundColor: "rgba(0, 0, 0, 0.58)",
  },
  content: {
    position: "relative",
    top: "auto",
    left: "auto",
    right: "auto",
    bottom: "auto",
    maxWidth: "480px",
    margin: "32px auto",
    padding: 0,
    border: 0,
    zIndex: 999,
    agree_box: false
  }
};

class CompanyPage extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      countries: [
        "usa",
        "china",
        "japan",
        "germany",
        "england",
        "france",
        "india",
        "italy",
        "brazil",
        "korea",
        "canada",
        "russia",
        "spain",
        "holland",
        "swiss"
      ],

      first_name: '',
      last_name: '',
      phone: '',
      company_name: '',
      country: 'USA',
      email: '',
      button_clicked: '',
      isSending: false
    }

    this.submitEnterprise = this.submitEnterprise.bind(this)
    this.onChange = this.onChange.bind(this)
    this.sendBuy = this.sendBuy.bind(this)
    this.validatingForm = this.validatingForm.bind(this)
    this.renderError = this.renderError.bind(this)
  }

  validatingForm() {
    if(this.state.first_name.trim().length < 2) {
        this.setState({
            error: {
                title: 'first_name',
                string: this.props.intl.formatMessage({ id: "validation.first_name" })
            },
            isSending: true
        })
        return false;
    }
    if(!EmailValidator.validate(this.state.email)) {
        this.setState({
            error: {
                title: 'email',
                string: this.props.intl.formatMessage({ id: "validation.email" })
            },
            isSending: true
        })
        return false;
    }
    if(!this.state.agree_box === true) {
        this.setState({
            error: {
                title: 'agree_box',
                string: this.props.intl.formatMessage({ id: "validation.agree" })
            },
            isSending: true
        })
        return false;
    }

    this.setState({
        error: null,
        isSending: false
    })
}

  sendBuy() {
    if(!this.state.agree_box === true) {
      return false;
    }
    if(!EmailValidator.validate(this.state.email)) {
      return false;
    }
    if(this.state.first_name.trim().length > 1) {
      this.setState({isSending: true})
      axios.post(this.props.data.site.siteMetadata.endpoints.log + this.props.token, {
        type_id: 9,
        data: {
          button_clicked: 'enterprise',
          email: this.state.email,
          first_name: this.state.first_name,
          last_name: this.state.last_name,
          phone: this.state.phone,
          company_name: this.state.company_name,
          country: this.state.country
        },
        timestamp: new Date().toISOString(),
        lang: this.props.intl.locale        
      }, {
        headers: {
          "Content-Type": "application/json",
          "Accept": "application/json",
        }
      })
        .then(response => {
          this.setState({isSending: false})
          alert(this.props.intl.formatMessage({id: `submitted`}))
        })
        .catch(e => {
          console.log(e)
          this.setState({isSending: false})
        })
        .then(() => {
          this.setState({isSending: false})
        })
    } else {
      alert(this.props.intl.formatMessage({id: `required`}))
    }
  }

  componentDidMount() {
    this.validatingForm();

    if(this.props.auth && this.props.auth.user && this.props.auth.user.email) {
      this.setState({
          email: this.props.auth.user.email
      })
    }

    if(this.props.auth && this.props.auth.user && this.props.auth.user.given_name) {
        this.setState({
            first_name: this.props.auth.user.given_name
        })
    }
  }
  
  onChange(e) {
    if(e.target.type == 'checkbox') {
      this.setState({
        [e.target.name]: e.target.checked
      }, () => {
        this.validatingForm()
    })
    } else {
        this.setState({
          [e.target.name]: e.target.value
      }, () => {
          this.validatingForm()
      })
    }
  }

  submitEnterprise() {
    const { first_name, last_name, phone, company_name, country, email } = this.state

    if(first_name.trim().length < 2 || last_name.trim().length < 2 || phone.trim().length < 2 || email.trim().length < 5) {
      return false;
    }

    window.open(`${this.props.data.site.siteMetadata.endpoints.enterprise}&first_name=${first_name}&last_name=${last_name}&phone=${phone}&company_name=${company_name}&country=${country}&email=${email}`, '_blank');
  }

  renderError(title) {
    if (this.state.error && this.state.error.title === title) {
        return <small>{ this.state.error.string }</small>
    } else {
        return null
    }
  }

  componentDidUpdate(prevProps) {
    if(JSON.stringify(this.props.auth) !== JSON.stringify(prevProps.auth)) {
        if(this.props.auth && this.props.auth.user && this.props.auth.user.email) {
            this.setState({
                email: this.props.auth.user.email
            })
        }
  
        if(this.props.auth && this.props.auth.user && this.props.auth.user.given_name) {
            this.setState({
                first_name: this.props.auth.user.given_name
            })
        }
    }
  }

  render() {
    return (
      <React.Fragment>
          <SEO
            title={this.props.intl.formatMessage({ id: "seo.plans_title" })}
            description={this.props.intl.formatMessage({ id: "seo.plans_description" })}
            lang={this.props.intl.locale}
          />
          <Header />
          <main id="app">
              <div className="container">
                <Link to="/plans" className="d-block mt-12">{this.props.intl.formatMessage({id: `enterprise.back`})}</Link>
  
                <h1 className="text-center mt-3">{this.props.intl.formatMessage({id: `enterprise.contact`})}</h1>
                <h2 className="text-center mb-12">{this.props.intl.formatMessage({id: `enterprise.enterprise`})}</h2>
  
                <div className="form">
                  <div className="container">
                      <h3 className="text-center">{this.props.intl.formatMessage({id: `enterprise.title`})}</h3>
  
                      <div className="columns">
                        <div className="column col-6 col-md-12 pr-5">
                          <div className="form-group">
                            <label className="form-label" htmlFor="input-example-1">{this.props.intl.formatMessage({id: `enterprise.first_name`})} *</label>
                            <input className="form-input" value={this.state.first_name} onChange={this.onChange} type="text" name="first_name" id="input-example-1" placeholder={this.props.intl.formatMessage({id: `enterprise.first_name`})} />
                            {this.renderError('first_name')}
                          </div>
                          <div className="form-group">
                            <label className="form-label" htmlFor="input-example-2">{this.props.intl.formatMessage({id: `enterprise.last_name`})}</label>
                            <input className="form-input" onChange={this.onChange} type="text" name="last_name" id="input-example-2" placeholder={this.props.intl.formatMessage({id: `enterprise.last_name`})} />
                          </div>
                          <div className="form-group">
                            <label className="form-label" htmlFor="input-example-3">{this.props.intl.formatMessage({id: `enterprise.phone`})}</label>
                            <input className="form-input" onChange={this.onChange} type="text" name="phone" id="input-example-3" placeholder={this.props.intl.formatMessage({id: `enterprise.phone`})} />
                          </div>
                        </div>
                        <div className="column col-6 col-md-12 pl-5">
                          <div className="form-group">
                            <label className="form-label" htmlFor="input-example-5">{this.props.intl.formatMessage({id: `enterprise.company`})}</label>
                            <input className="form-input" onChange={this.onChange} type="text" name="company_name" id="input-example-5" placeholder={this.props.intl.formatMessage({id: `enterprise.company`})} />
                          </div>
                          <div className="form-group">
                            <label className="form-label" htmlFor="input-example-3">{this.props.intl.formatMessage({id: `enterprise.country`})}</label>
                            <select className="form-select" onChange={this.onChange} name="country">
                              {
                                this.state.countries.map((country, index) => {
                                  return (
                                    <option key={`country_${index}`}>{this.props.intl.formatMessage({id: `countries.${country}`})}</option>
                                  )
                                })
                              }
                            </select>
                          </div>
                          <div className="form-group">
                            <label className="form-label" htmlFor="input-example-4">Email *</label>
                            <input className="form-input" value={this.state.email} onChange={this.onChange} name="email" type="text" id="input-example-4" placeholder="Email" />
                            {this.renderError('email')}
                          </div>
                        </div>
                        <div className="column col-12 text-left mt-6">
                        <div>
              <label className="form-radio mr-5 label-static">
              <input type="checkbox" checked={this.state.agree_box === true} value={this.state.agree_box === true} name="agree_box" onChange={this.onChange} />
              <i className="form-icon"></i> <FormattedHTMLMessage id="agree" /> <u><a href="/terms" className="text-white" target="_blank"><FormattedHTMLMessage id="terms" /></a></u></label>
              {this.renderError('agree_box')}
            </div>
            </div>
            <div className="column col-12 text-center mt-6">
                          <button disabled={this.state.isSending} onClick={this.sendBuy} className="btn btn-primary btn-lg px-6">{this.props.intl.formatMessage({id: `submit`})}</button>
                        </div>
                      </div>
                  </div>
                </div>
              </div>
          </main>
          <Footer />
      </React.Fragment>
    )
  }
}

export const query = graphql`
  query {
    site {
      siteMetadata {
          endpoints {
              signin
              signup
              purchase
              subscribe
              enterprise
              buy_enterprise
              log
          }
      }
    }
  }
`

const SuperCompanyPage = (props) => {
  const intl = useIntl();
  const auth = useAuth0();

  return (
    <CompanyPage intl={intl} {...props} auth={auth} />
  )
}

const mapStateToProps = ({ token }) => {
  return { token }
}

const mapDispatchToProps = dispatch => {
  return {}
}

export default connect(mapStateToProps, mapDispatchToProps)(SuperCompanyPage)
import React from "react"
import SEO from "../components/seo"
import Header from './../components/Header'
import Footer from './../components/Footer'
import Toggle from 'react-toggle'
import { Link, FormattedHTMLMessage, useIntl } from 'gatsby-plugin-intl';
import Media from 'react-media';
import "react-toggle/style.css"
import Modal from 'react-modal';
import axios from "axios";
import * as EmailValidator from 'email-validator';
import { connect } from 'react-redux'
import { graphql } from "gatsby"
import ReactTooltip from 'react-tooltip';
import { useAuth0 } from '@auth0/auth0-react';

Modal.setAppElement('#___gatsby');

const modalStyles = {
  overlay: {
    backgroundColor: "rgba(0, 0, 0, 0.58)",
  },
  content: {
    position: "relative",
    top: "auto",
    left: "auto",
    right: "auto",
    bottom: "auto",
    maxWidth: "480px",
    margin: "32px auto",
    padding: 0,
    border: 0,
    zIndex: 999,
    agree_box: false
  },
};

class PlansPage extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      term: 'yearly',
      totalPrice: 0,
      products: [],
      prices: {
        constructor: {
          ru: 2500,
          en: 50
        },
        showroom: {
          ru: 1500,
          en: 35
        },
        education: {
          ru: 1000,
          en: 20
        },
        community: {
          ru: 500,
          en: 10
        },
      },
      plans: {
        monthly: {
          en: {
            byDemand: 20,
            enterpreneur: 100,
            symbolLeft: '$',
            symbolRight: '',
            code: 'USD'
          },
          ru: {
            byDemand: 1499,
            enterpreneur: 4990,
            symbolLeft: '',
            symbolRight: '₽',
            code: 'РУБ'
          }
        },
        quarter: {
          en: {
            byDemand: 20,
            enterpreneur: 80,
            symbolLeft: '$',
            symbolRight: '',
            code: 'USD'
          },
          ru: {
            byDemand: 1499,
            enterpreneur: 4200,
            symbolLeft: '', 
            symbolRight: '₽',
            code: 'РУБ'
          }
        },
        half: {
          en: {
            byDemand: 20,
            enterpreneur: 80,
            symbolLeft: '$',
            symbolRight: '',
            code: 'USD'
          },
          ru: {
            byDemand: 1499,
            enterpreneur: 4000,
            symbolLeft: '', 
            symbolRight: '₽',
            code: 'РУБ'
          }
        },
        yearly: {
          en: {
            byDemand: 20,
            enterpreneur: 80,
            symbolLeft: '$',
            symbolRight: '',
            code: 'USD'
          },
          ru: {
            byDemand: 1499,
            enterpreneur: 3500,
            symbolLeft: '', 
            symbolRight: '₽',
            code: 'РУБ'
          }
        }
      },
      cardOne: false,
      cardSecond: false,
      cardThird: false,

      cardOneHeight: 0,
      cardTwoHeight: 0,
      cardThirdHeight: 0,

      first_name: '',
      email: '',
      modalOpenEntre: false,
      modalOpenRequest: false,
      button_clicked: '',
      isSending: false
    }

    this.togglePlan = this.togglePlan.bind(this)
    this.toggleCard = this.toggleCard.bind(this)
    this.addItem = this.addItem.bind(this)
    this.recalcPrice = this.recalcPrice.bind(this)

    this.onChange = this.onChange.bind(this)
    this.openModalEntre = this.openModalEntre.bind(this)
    this.closeModalEntre = this.closeModalEntre.bind(this)
    this.sendEntre = this.sendEntre.bind(this)
    this.openModalRequest = this.openModalRequest.bind(this)
    this.closeModalRequest = this.closeModalRequest.bind(this)
    this.sendRequest = this.sendRequest.bind(this)
    this.validatingForm = this.validatingForm.bind(this)
    this.renderError = this.renderError.bind(this)
  }

  sendEntre() {
    if(!this.state.agree_box === true) {
      return false;
    }
    if(!EmailValidator.validate(this.state.email)) {
      return false;
    }
    if(this.state.first_name.trim().length > 1) {
      this.setState({isSending: true})
      axios.post(this.props.data.site.siteMetadata.endpoints.log + this.props.token, {
        type_id: 8,
        data: {
          button_clicked: 'entrepreneur',
          email: this.state.email,
          first_name: this.state.first_name,
          term: this.state.term
        },
        timestamp: new Date().toISOString(),
        lang: this.props.intl.locale
      }, {
        headers: {
          "Content-Type": "application/json",
          "Accept": "application/json",
        }
      })
        .then(response => {
          this.closeModalEntre()
          this.setState({isSending: false})
        })
        .catch(e => {
          console.log(e)
          this.setState({isSending: false})
        })
        .then(() => {
          this.closeModalEntre()
          this.setState({isSending: false})
        })
    }
  }

  sendRequest() {
    if(!this.state.agree_box === true) {
      return false;
    }
    if(!EmailValidator.validate(this.state.email)) {
      return false;
    }
    if(this.state.first_name.trim().length > 1) {
      this.setState({isSending: true})
      axios.post(this.props.data.site.siteMetadata.endpoints.log + this.props.token, {
        type_id: 7,
        data: {
          button_clicked: 'request',
          email: this.state.email,
          first_name: this.state.first_name,
          products: this.state.products,
          total_price: this.state.totalPrice,
          term: this.state.term
        },
        timestamp: new Date().toISOString(),
        lang: this.props.intl.locale
      }, {
        headers: {
          "Content-Type": "application/json",
          "Accept": "application/json",
        }
      })
        .then(response => {
          this.closeModalRequest()
          this.setState({isSending: false})
        })
        .catch(e => {
          console.log(e)
          this.setState({isSending: false})
        })
        .then(() => {
          this.closeModalRequest()
          this.setState({isSending: false})
        })
    }
  }

  onChange(e) {
    if(e.target.type == 'checkbox') {
      this.setState({
        [e.target.name]: e.target.checked
      }, () => {
        this.validatingForm()
      })
    } else {
      this.setState({
        [e.target.name]: e.target.value
      }, () => {
        this.validatingForm()
      })
    }
  }

  openModalEntre() {
    this.setState({
      button_clicked: 'enterpreneur',
      modalOpenEntre: true
    }, () => {
      this.validatingForm()
    })
  }

  closeModalEntre() {
    this.setState({modalOpenEntre: false})
  };

  openModalRequest() {
    if(this.state.products.length < 1 || this.state.totalPrice < 1) {
      alert(this.props.intl.messages["price_page.choose_alert"])
      return false
    }
    this.setState({
      button_clicked: 'enterpreneur',
      modalOpenRequest: true
    }, () => {
      this.validatingForm()
    })
  }

  closeModalRequest() {
    this.setState({modalOpenRequest: false})
  };

  togglePlan(e) {
    this.setState({
      term: e.target.value
    })
  }

  toggleCard(card) {
    this.setState({
      [card]: !this.state[card]
    })
  }

  recalcPrice() {
    let summary = 0
    this.state.products.forEach(item => {
      summary += parseInt(this.state.prices[item.label][this.props.intl.locale])
    })

    this.setState({
      totalPrice: summary
    })
  }

  addItem(item) {
    let newArray = []
    const idx = this.state.products.findIndex(it => it.label == item)
    if(idx != -1) {
      newArray = [
        ...this.state.products.slice(0, idx),
        ...this.state.products.slice(idx + 1)
      ]
    } else {
      let newProduct = {
        label: item,
        price: this.state.prices[item][this.props.intl.locale]
      }
      newArray = [...this.state.products, newProduct]
    }

    this.setState({
      products: newArray
    }, () => {
      this.recalcPrice()
      console.log(this.state.products)
    })
  }

  validatingForm() {
    if(this.state.first_name.trim().length < 2) {
        this.setState({
            error: {
                title: 'first_name',
                string: this.props.intl.formatMessage({ id: "validation.first_name" })
            },
            isSending: true
        })
        return false;
    }
    if(!EmailValidator.validate(this.state.email)) {
        this.setState({
            error: {
                title: 'email',
                string: this.props.intl.formatMessage({ id: "validation.email" })
            },
            isSending: true
        })
        return false;
    }
    if(!this.state.agree_box === true) {
        this.setState({
            error: {
                title: 'agree_box',
                string: this.props.intl.formatMessage({ id: "validation.agree" })
            },
            isSending: true
        })
        return false;
    }

    this.setState({
        error: null,
        isSending: false
    })
  }

  renderError(title) {
    if (this.state.error && this.state.error.title === title) {
        return <small>{ this.state.error.string }</small>
    } else {
        return null
    }
  }

  componentDidMount() {
    if(this.props.auth && this.props.auth.user && this.props.auth.user.email) {
      this.setState({
          email: this.props.auth.user.email
      })
    }

    if(this.props.auth && this.props.auth.user && this.props.auth.user.given_name) {
        this.setState({
            first_name: this.props.auth.user.given_name
        })
    }
  }

  componentDidUpdate(prevProps) {
    if(JSON.stringify(this.props.auth) !== JSON.stringify(prevProps.auth)) {
        if(this.props.auth && this.props.auth.user && this.props.auth.user.email) {
            this.setState({
                email: this.props.auth.user.email
            })
        }
  
        if(this.props.auth && this.props.auth.user && this.props.auth.user.given_name) {
            this.setState({
                first_name: this.props.auth.user.given_name
            })
        }
    }
  }

  render() {
    return (
      <React.Fragment>
        <SEO
          title={this.props.intl.formatMessage({ id: "seo.plans_title" })}
          description={this.props.intl.formatMessage({ id: "seo.plans_description" })}
          lang={this.props.intl.locale}
        />
        <Header />
        <main id="app">
            <div className="container">
              <h1 className="text-center my-12"><FormattedHTMLMessage id="price_page.title" /></h1>

              <div className="text-center mb-12">
                <div className="d-flex justify-content-center mb-5">
                  <label className="form-radio mr-5">
                    <input type="radio" name="gender" value="monthly" checked={this.state.term === 'monthly'} onChange={this.togglePlan} />
                    <i className="form-icon"></i> <span className="ml-2"><span className="font-mono">1</span> M</span>
                  </label>
                  <label className="form-radio mr-5">
                    <input type="radio" name="gender" value="quarter" checked={this.state.term === 'quarter'} onChange={this.togglePlan} />
                    <i className="form-icon"></i> <span className="ml-2"><span className="font-mono">3</span> M</span>
                  </label>
                  <label className="form-radio mr-5">
                    <input type="radio" name="gender" value="half" checked={this.state.term === 'half'} onChange={this.togglePlan} />
                    <i className="form-icon"></i> <span className="ml-2"><span className="font-mono">6</span> M</span>
                  </label>
                  <label className="form-radio">
                    <input type="radio" name="gender" value="yearly" checked={this.state.term === 'yearly'} onChange={this.togglePlan} />
                    <i className="form-icon"></i> <span className="ml-2"><span className="font-mono">12</span> M</span>
                  </label>
                </div>
              </div>

              <span className="display-1122">
                <div>
                  <div className={"text-center flipper " + (this.state.cardSecond ? 'flipped' : '')}>
                    <div className="price-card price-card__center flipper-front">
                      <div>
                        <div className="price-card__title"><FormattedHTMLMessage id="price_page.entre" /></div>
                        <img src={`../../entre.svg`} className="img-responsive price-image" alt="" />
                      </div>
                      <div>
                        <div className="price-card__price">{this.state.plans[this.state.term][this.props.intl.locale].symbolLeft}<span className="font-mono">{ this.state.plans[this.state.term][this.props.intl.locale].enterpreneur }</span> {this.state.plans[this.state.term][this.props.intl.locale].symbolRight}</div>
                        {/* <div className="price-card__subprice">{this.state.plans[this.state.term][this.props.intl.locale].code}/<FormattedHTMLMessage id="price_page.month_1" />/<FormattedHTMLMessage id="price_page.user_1" /></div> */}
                        <div className="price-card__subprice"><FormattedHTMLMessage id="price_page.user_month" /></div>
                        {/* <a target="_blank" href={this.props.data.site.siteMetadata.endpoints.subscribe} className="btn btn-xl btn-primary w-100 my-3"><FormattedHTMLMessage id="price_page.subscribe" /></a> */}
                        <button onClick={this.openModalEntre} className="btn btn-xl btn-primary w-100 my-3"><FormattedHTMLMessage id="price_page.subscribe" /></button>
                        
                        <ul className="price__features">
                          <li><FormattedHTMLMessage id="price_page.items.6" /></li>
                          <li><FormattedHTMLMessage id="price_page.items.7" /></li>
                          <li><FormattedHTMLMessage id="price_page.items.8" /></li>
                          <li><FormattedHTMLMessage id="price_page.items.9" /></li>
                        </ul>
                      </div>

                      <div className="price-label"><FormattedHTMLMessage id="price_page.most" /></div>
                    </div>
                    <div className="price-card price-card__center flipper-back">
                      <div>
                        <div className="price-card__title"><FormattedHTMLMessage id="price_page.entre" /></div>
                        <div  className="price_about"><FormattedHTMLMessage id="price_page.entre_body" /></div>
                      </div>
                      <div>
                        <div className="price-card__price">{this.state.plans[this.state.term][this.props.intl.locale].symbolLeft}<span className="font-mono">{ this.state.plans[this.state.term][this.props.intl.locale].enterpreneur }</span> {this.state.plans[this.state.term][this.props.intl.locale].symbolRight}</div>
                        {/* <div className="price-card__subprice">{this.state.plans[this.state.term][this.props.intl.locale].code}/<FormattedHTMLMessage id="price_page.month_1" />/<FormattedHTMLMessage id="price_page.user_1" /></div> */}
                        <div className="price-card__subprice"><FormattedHTMLMessage id="price_page.user_month" /></div>
                        <a target="_blank" href={this.props.data.site.siteMetadata.endpoints.subscribe} className="btn btn-xl w-100 my-3"><FormattedHTMLMessage id="price_page.subscribe" /></a>
                      </div>

                      <div className="price-label"><FormattedHTMLMessage id="price_page.most" /></div>
                    </div>
                  </div>

                  <div className={"text-right flipper short " + (this.state.cardOne ? 'flipped' : '')}>
                    <div className="price-card price-card__left flipper-front">
                      <div>
                        <div className="price-card__title"><FormattedHTMLMessage id="price_page.pag" /></div>
                        <img src={`../../demand.svg`} className="img-responsive price-image" alt="" />
                      </div>
                      <div>
                        <div className="price-card__price">{this.state.plans[this.state.term][this.props.intl.locale].symbolLeft}
                        {/* { this.state.plans[this.state.term][this.props.intl.locale].byDemand } */}
                        <span className="font-mono">{ this.state.totalPrice }</span> {this.state.plans[this.state.term][this.props.intl.locale].symbolRight}</div>
                        {/* <div className="price-card__subprice">{this.state.plans[this.state.term][this.props.intl.locale].code}/1 <FormattedHTMLMessage id="price_page.credit" /></div> */}
                        <button onClick={this.openModalRequest} className="btn btn-xl btn-primary w-100 my-3"><FormattedHTMLMessage id="price_page.subscribe" /></button>
                        {/* <a target="_blank" href={this.props.data.site.siteMetadata.endpoints.subscribe} className="btn btn-xl w-100 my-3"><FormattedHTMLMessage id="price_page.subscribe" /></a> */}

                        <div className="column col-4 col-sm-12 text-left">
                          <div className="text-center"><small className="text-muted"><FormattedHTMLMessage id="price_page.choose_tools" /></small></div>
                          <div className="custom-features">
                            <label className="form-radio mr-5">
                              <input type="checkbox" onChange={() => this.addItem('constructor')} />
                              <i className="form-icon"></i> <span><FormattedHTMLMessage id="price_page.items.1" /></span> - <span className="font-mono__relaxed">{this.state.plans[this.state.term][this.props.intl.locale].symbolLeft} { this.state.prices['constructor'][this.props.intl.locale] } { this.state.plans[this.state.term][this.props.intl.locale].symbolRight }</span>
                            </label>
                            <label className="form-radio mr-5">
                              <input type="checkbox" onChange={() => this.addItem('showroom')} />
                              <i className="form-icon"></i> <span><FormattedHTMLMessage id="price_page.items.2" /></span> - <span className="font-mono__relaxed">{this.state.plans[this.state.term][this.props.intl.locale].symbolLeft} { this.state.prices['showroom'][this.props.intl.locale] } { this.state.plans[this.state.term][this.props.intl.locale].symbolRight }</span>
                            </label>
                            <label className="form-radio mr-5">
                              <input type="checkbox" onChange={() => this.addItem('education')} />
                              <i className="form-icon"></i> <span><FormattedHTMLMessage id="price_page.items.3" /></span> - <span className="font-mono__relaxed">{this.state.plans[this.state.term][this.props.intl.locale].symbolLeft} { this.state.prices['education'][this.props.intl.locale] } { this.state.plans[this.state.term][this.props.intl.locale].symbolRight }</span>
                            </label>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>

                  <div className={"text-left flipper short " + (this.state.cardThird ? 'flipped' : '')}>
                    <div className="price-card price-card__right flipper-front">
                      <div>
                        <div className="price-card__title"><FormattedHTMLMessage id="price_page.enterprise" /></div>
                        <img src={`../../enterprise.svg`} className="img-responsive price-image" alt="" />
                      </div>
                      <div>
                        <div className="price-card__price"><FormattedHTMLMessage id="price_page.talk" /></div>
                        <div className="price-card__subprice"></div>
                        <Link to="/plans/enterprise" className="btn btn-xl btn-primary w-100 my-3"><FormattedHTMLMessage id="price_page.contact" /></Link>
                        
                        <ul className="price__features">
                          <li><FormattedHTMLMessage id="price_page.items.15" /></li>
                          <li><FormattedHTMLMessage id="price_page.items.16" /></li>
                          {/* <li><FormattedHTMLMessage id="price_page.items.17" /></li>
                          <li><FormattedHTMLMessage id="price_page.items.18" /></li> */}
                        </ul>
                      </div>
                    </div>
                    <div className="price-card price-card__left flipper-back">
                      <div>
                        <div className="price-card__title"><FormattedHTMLMessage id="price_page.enterprise" /></div>
                        <div  className="price_about"><FormattedHTMLMessage id="price_page.enter_body" /></div>
                      </div>
                      <div>
                        <div className="price-card__price"><FormattedHTMLMessage id="price_page.talk" /></div>
                        <div className="price-card__subprice"></div>
                        <Link to="/plans/enterprise" className="btn btn-xl w-100 my-3"><FormattedHTMLMessage id="price_page.contact" /></Link>
                      </div>
                    </div>
                  </div>
                </div>
              </span>

              <span className="hide-1122">
              <div className="columns">
                <div className={"column col-4 col-sm-12 col-md-6 text-right flipper short " + (this.state.cardOne ? 'flipped' : '')}>
                  <div className="price-card price-card__left flipper-front">
                    <div>
                      <div className="price-card__title"><FormattedHTMLMessage id="price_page.pag" /></div>
                      <img src={`../../demand.svg`} className="img-responsive price-image" alt="" />
                    </div>
                    <div>
                      <div className="price-card__price">{this.state.plans[this.state.term][this.props.intl.locale].symbolLeft}
                      {/* { this.state.plans[this.state.term][this.props.intl.locale].byDemand } */}
                      <span className="font-mono">{ this.state.totalPrice }</span> {this.state.plans[this.state.term][this.props.intl.locale].symbolRight}</div>
                      {/* <div className="price-card__subprice">{this.state.plans[this.state.term][this.props.intl.locale].code}/1 <FormattedHTMLMessage id="price_page.credit" /></div> */}
                      <button onClick={this.openModalRequest} className="btn btn-xl btn-primary w-100 my-3"><FormattedHTMLMessage id="price_page.subscribe" /></button>
                      {/* <a target="_blank" href={this.props.data.site.siteMetadata.endpoints.subscribe} className="btn btn-xl w-100 my-3"><FormattedHTMLMessage id="price_page.subscribe" /></a> */}
                      <button className="price-button" onClick={() => this.toggleCard('cardOne')}><FormattedHTMLMessage id="price_page.more" /></button>
                    </div>
                  </div>
                  <div className="price-card price-card__left flipper-back">
                    <div>
                      <div className="price-card__title"><FormattedHTMLMessage id="price_page.pag" /></div>
                      <div  className="price_about"><FormattedHTMLMessage id="price_page.pag_body" /></div>
                    </div>
                    <div>
                      <div className="price-card__price">{this.state.plans[this.state.term][this.props.intl.locale].symbolLeft}
                      {/* { this.state.plans[this.state.term][this.props.intl.locale].byDemand } */}
                      <span className="font-mono">{ this.state.totalPrice }</span> {this.state.plans[this.state.term][this.props.intl.locale].symbolRight}</div>
                      {/* <div className="price-card__subprice">{this.state.plans[this.state.term][this.props.intl.locale].code}/1 <FormattedHTMLMessage id="price_page.credit" /></div> */}
                      <button onClick={this.openModalRequest} className="btn btn-xl btn-primary w-100 my-3"><FormattedHTMLMessage id="price_page.subscribe" /></button>
                      {/* <a target="_blank" href={this.props.data.site.siteMetadata.endpoints.subscribe} className="btn btn-xl w-100 my-3"><FormattedHTMLMessage id="price_page.subscribe" /></a> */}
                      <button className="price-button" onClick={() => this.toggleCard('cardOne')}><FormattedHTMLMessage id="price_page.back" /></button>
                    </div>
                  </div>
                </div>
                <div className={"column col-4 col-sm-12 col-md-6 text-center flipper " + (this.state.cardSecond ? 'flipped' : '')}>
                  <div className="price-card price-card__center flipper-front">
                    <div>
                      <div className="price-card__title"><FormattedHTMLMessage id="price_page.entre" /></div>
                      <img src={`../../entre.svg`} className="img-responsive price-image" alt="" />
                    </div>
                    <div>
                      <div className="price-card__price">{this.state.plans[this.state.term][this.props.intl.locale].symbolLeft}<span className="font-mono">{ this.state.plans[this.state.term][this.props.intl.locale].enterpreneur }</span> {this.state.plans[this.state.term][this.props.intl.locale].symbolRight}</div>
                      {/* <div className="price-card__subprice">{this.state.plans[this.state.term][this.props.intl.locale].code}/<FormattedHTMLMessage id="price_page.month_1" />/<FormattedHTMLMessage id="price_page.user_1" /></div> */}
                      <div className="price-card__subprice"><FormattedHTMLMessage id="price_page.user_month" /></div>
                      {/* <a target="_blank" href={this.props.data.site.siteMetadata.endpoints.subscribe} className="btn btn-xl btn-primary w-100 my-3"><FormattedHTMLMessage id="price_page.subscribe" /></a> */}
                      <button onClick={this.openModalEntre} className="btn btn-xl btn-primary w-100 my-3"><FormattedHTMLMessage id="price_page.subscribe" /></button>
                      <button className="price-button text-white" onClick={() => this.toggleCard('cardSecond')}><FormattedHTMLMessage id="price_page.more" /></button>
                    </div>

                    <div className="price-label"><FormattedHTMLMessage id="price_page.most" /></div>
                  </div>
                  <div className="price-card price-card__center flipper-back">
                    <div>
                      <div className="price-card__title"><FormattedHTMLMessage id="price_page.entre" /></div>
                      <div  className="price_about"><FormattedHTMLMessage id="price_page.entre_body" /></div>
                    </div>
                    <div>
                      <div className="price-card__price">{this.state.plans[this.state.term][this.props.intl.locale].symbolLeft}<span className="font-mono">{ this.state.plans[this.state.term][this.props.intl.locale].enterpreneur }</span> {this.state.plans[this.state.term][this.props.intl.locale].symbolRight}</div>
                      {/* <div className="price-card__subprice">{this.state.plans[this.state.term][this.props.intl.locale].code}/<FormattedHTMLMessage id="price_page.month_1" />/<FormattedHTMLMessage id="price_page.user_1" /></div> */}
                      <div className="price-card__subprice"><FormattedHTMLMessage id="price_page.user_month" /></div>
                      {/* <a target="_blank" href={this.props.data.site.siteMetadata.endpoints.subscribe} className="btn btn-xl btn-primary w-100 my-3"><FormattedHTMLMessage id="price_page.subscribe" /></a> */}
                      <button onClick={this.openModalEntre} className="btn btn-xl btn-primary w-100 my-3"><FormattedHTMLMessage id="price_page.subscribe" /></button>
                      <button className="price-button text-white" onClick={() => this.toggleCard('cardSecond')}><FormattedHTMLMessage id="price_page.back" /></button>
                    </div>

                    <div className="price-label"><FormattedHTMLMessage id="price_page.most" /></div>
                  </div>
                </div>
                <div className={"column col-4 col-sm-12 col-md-6 text-left flipper short " + (this.state.cardThird ? 'flipped' : '')}>
                  <div className="price-card price-card__right flipper-front">
                    <div>
                      <div className="price-card__title"><FormattedHTMLMessage id="price_page.enterprise" /></div>
                      <img src={`../../enterprise.svg`} className="img-responsive price-image" alt="" />
                    </div>
                    <div>
                      <div className="price-card__price"><FormattedHTMLMessage id="price_page.talk" /></div>
                      <div className="price-card__subprice"></div>
                      <Link to="/plans/enterprise" className="btn btn-xl btn-primary w-100 my-3"><FormattedHTMLMessage id="price_page.contact" /></Link>
                      <button className="price-button" onClick={() => this.toggleCard('cardThird')}><FormattedHTMLMessage id="price_page.more" /></button>
                    </div>
                  </div>
                  <div className="price-card price-card__left flipper-back">
                    <div>
                      <div className="price-card__title"><FormattedHTMLMessage id="price_page.enterprise" /></div>
                      <div  className="price_about"><FormattedHTMLMessage id="price_page.enter_body" /></div>
                    </div>
                    <div>
                      <div className="price-card__price"><FormattedHTMLMessage id="price_page.talk" /></div>
                      <div className="price-card__subprice"></div>
                      <Link to="/plans/enterprise" className="btn btn-xl btn-primary w-100 my-3"><FormattedHTMLMessage id="price_page.contact" /></Link>
                      <button className="price-button" onClick={() => this.toggleCard('cardThird')}><FormattedHTMLMessage id="price_page.back" /></button>
                    </div>
                  </div>
                </div>

                <div className="column col-4 col-sm-12 text-left">
                  <div className="text-center"><small className="text-muted"><FormattedHTMLMessage id="price_page.choose_tools" /></small></div>
                  <div className="custom-features">
                    <label className="form-radio mr-5" data-tip={this.props.intl.formatMessage({ id: "price_page.items.1_tooltip" })} data-effect="solid">
                      <input type="checkbox" onChange={() => this.addItem('constructor')} />
                      <i className="form-icon"></i> <span><FormattedHTMLMessage id="price_page.items.1" /></span> - <span className="font-mono__relaxed">{this.state.plans[this.state.term][this.props.intl.locale].symbolLeft} { this.state.prices['constructor'][this.props.intl.locale] } { this.state.plans[this.state.term][this.props.intl.locale].symbolRight }</span>
                    </label>
                    <label className="form-radio mr-5" data-tip={this.props.intl.formatMessage({ id: "price_page.items.2_tooltip" })} data-effect="solid">
                      <input type="checkbox" onChange={() => this.addItem('showroom')} />
                      <i className="form-icon"></i> <span><FormattedHTMLMessage id="price_page.items.2" /></span> - <span className="font-mono__relaxed">{this.state.plans[this.state.term][this.props.intl.locale].symbolLeft} { this.state.prices['showroom'][this.props.intl.locale] } { this.state.plans[this.state.term][this.props.intl.locale].symbolRight }</span>
                    </label>
                    <label className="form-radio mr-5" data-tip={this.props.intl.formatMessage({ id: "price_page.items.3_tooltip" })} data-effect="solid">
                      <input type="checkbox" onChange={() => this.addItem('education')} />
                      <i className="form-icon"></i> <span><FormattedHTMLMessage id="price_page.items.3" /></span> - <span className="font-mono__relaxed">{this.state.plans[this.state.term][this.props.intl.locale].symbolLeft} { this.state.prices['education'][this.props.intl.locale] } { this.state.plans[this.state.term][this.props.intl.locale].symbolRight }</span>
                    </label>
                  </div>
                </div>
                <div className="column col-4 col-sm-12 text-center">
                  <ul className="price__features">
                    <li><FormattedHTMLMessage id="price_page.items.6" /></li>
                    <li><FormattedHTMLMessage id="price_page.items.7" /></li>
                    <li><FormattedHTMLMessage id="price_page.items.8" /></li>
                    <li><FormattedHTMLMessage id="price_page.items.9" /></li>
                  </ul>
                </div>
                <div className="column col-4 col-sm-12 text-left">
                  <ul className="price__features">
                    <li><FormattedHTMLMessage id="price_page.items.15" /></li>
                    <li><FormattedHTMLMessage id="price_page.items.16" /></li>
                  </ul>
                </div>
              </div>
              </span>
            </div>
            <ReactTooltip />
        </main>
        <Footer />

        <Modal
          isOpen={this.state.modalOpenEntre}
          onRequestClose={this.closeModalEntre}
          style={modalStyles}
          contentLabel="Modal"
          closeTimeoutMS={500}
        >
          <div className="modal__body">
            <div className="modal__name"><FormattedHTMLMessage id="modal.enter_email_plans" /></div>
            <div className="form-group mt-5">
              <label className="form-label" htmlFor="input-example-4"><FormattedHTMLMessage id="modal.name" /></label>
              <input className="form-input" value={this.state.first_name} onChange={this.onChange} name="first_name" type="text" id="input-example-4" placeholder={this.props.intl.formatMessage({id: `modal.name`})} />
              {this.renderError('first_name')}
            </div>
            <div className="form-group mb-5">
              <label className="form-label" htmlFor="input-example-4">Email</label>
              <input className="form-input" value={this.state.email} onChange={this.onChange} name="email" type="text" id="input-example-4" placeholder="Email" />
              {this.renderError('email')}
            </div>

            <div>
              <label className="form-radio mr-5 label-static">
              <input type="checkbox" checked={this.state.agree_box === true} value={this.state.agree_box === true} name="agree_box" onChange={this.onChange} />
              <i className="form-icon"></i> <FormattedHTMLMessage id="agree" /> <u><a href="/terms" target="_blank"><FormattedHTMLMessage id="terms" /></a></u></label>
              {this.renderError('agree_box')}
            </div>

            <button disabled={this.state.isSending} className="btn btn-primary font-bold px-6 my-5" onClick={this.sendEntre}>{this.props.intl.formatMessage({id: `submit`})}</button>
          </div>
        </Modal>

        <Modal
          isOpen={this.state.modalOpenRequest}
          onRequestClose={this.closeModalRequest}
          style={modalStyles}
          contentLabel="Modal"
          closeTimeoutMS={500}
        >
          <div className="modal__body">
            <div className="modal__name"><FormattedHTMLMessage id="modal.enter_email_plans" /></div>
            <div className="form-group mt-5">
              <label className="form-label" htmlFor="input-example-4"><FormattedHTMLMessage id="modal.name" /></label>
              <input className="form-input" value={this.state.first_name} onChange={this.onChange} name="first_name" type="text" id="input-example-4" placeholder={this.props.intl.formatMessage({id: `modal.name`})} />
              {this.renderError('first_name')}
            </div>
            <div className="form-group mb-5">
              <label className="form-label" htmlFor="input-example-4">Email</label>
              <input className="form-input" value={this.state.email} onChange={this.onChange} name="email" type="text" id="input-example-4" placeholder="Email" />
              {this.renderError('email')}
            </div>

            <div>
              <label className="form-radio mr-5 label-static">
              <input type="checkbox" checked={this.state.agree_box === true} value={this.state.agree_box === true} name="agree_box" onChange={this.onChange} />
              <i className="form-icon"></i> <FormattedHTMLMessage id="agree" /> <u><a href="/terms" target="_blank"><FormattedHTMLMessage id="terms" /></a></u></label>
              {this.renderError('agree_box')}
            </div>

            <button disabled={this.state.isSending} className="btn btn-primary font-bold px-6 my-5" onClick={this.sendRequest}>{this.props.intl.formatMessage({id: `submit`})}</button>
          </div>
        </Modal>
      </React.Fragment>
    )
  }
}

export const query = graphql`
  query {
    site {
      siteMetadata {
          endpoints {
              signin
              signup
              purchase
              subscribe
              enterprise
              buy_entre
              buy_request
              log
          }
      }
    }
  }
`

const SuperPlansPage = (props) => {
  const intl = useIntl();
  const auth = useAuth0();

  return (
      <PlansPage intl={intl} {...props} auth={auth} />
  )
}


const mapStateToProps = ({ token }) => {
  return { token }
}

const mapDispatchToProps = dispatch => {
  return {}
}

export default connect(mapStateToProps, mapDispatchToProps)(SuperPlansPage)
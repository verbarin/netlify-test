import React from "react"
import SEO from "../components/seo"

import { FormattedHTMLMessage, Link, useIntl } from 'gatsby-plugin-intl';

import Header from '../components/Header'
import Footer from '../components/Footer'

import { graphql } from "gatsby"

class SignupPage extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      modalOpen: false,
      currentMember: null,
      members: this.props.data.site.siteMetadata.members
    }

    this.openModal = this.openModal.bind(this)
    this.closeModal = this.closeModal.bind(this)
  }

  openModal(member) {
    this.setState({
      currentMember: member,
      modalOpen: true
    })
  }

  closeModal() {
    this.setState({modalOpen: false})
  };

  render() {
    return (
      <React.Fragment>
        <SEO
          title="Sign Up"
          description="Sign Up"
          lang={this.props.intl.locale}
        />
        <Header />
        <main id="app">
          <div className="container">
            <h1 className="text-center my-12"><FormattedHTMLMessage id="signup_page.title" /></h1>
  
            <div className="columns justify-content-center text-center">
              <div className="column col-4">
                    <button className="btn btn-primary btn-lg font-bold w-100 mb-4"><FormattedHTMLMessage id="signup_page.google" /></button>
                    <button className="btn btn-outline-black font-bold btn-lg w-100"><FormattedHTMLMessage id="signup_page.apple" /></button>

                    <hr className="hr mt-6 mb-3" />

                    <div className="form-group text-left">
                        <label className="form-label text-semi" htmlFor="input-example-1">Email</label>
                        <input className="form-input input-lg" type="text" id="input-example-1" placeholder="Email" />
                    </div>

                  <button className="btn btn-outline-third btn-lg font-bold w-100"><FormattedHTMLMessage id="signup_page.email" /></button>

                  <Link to="/reset" className="d-block text-semi text-underline my-6"><FormattedHTMLMessage id="signup_page.forgot" /></Link>
              </div>
            </div>

            <div className="columns justify-content-center text-center mt-12">
                <div className="column col-8 text-semi">
                    <FormattedHTMLMessage id="signup_page.info" />
                </div>
            </div>
        </div>
        </main>
        <Footer />
      </React.Fragment>
    )
  }
}

export const query = graphql`
  query {
    site {
      siteMetadata {
        members {
          en {
            name
            title
            photo
            color
            details {
              title
              links {
                icon
                url
              }
              about
            }
          }
          ru {
            name
            title
            photo
            color
            details {
              title
              links {
                icon
                url
              }
              about
            }
          }
        }
      }
    }
  }
`

const SuperSignupPage = (props) => {
  const intl = useIntl();

  return (
    <SignupPage intl={intl} {...props} />
  )
}

export default SuperSignupPage
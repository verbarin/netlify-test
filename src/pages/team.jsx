import React from "react"
import SEO from "../components/seo"
import Modal from 'react-modal';

import { FormattedHTMLMessage, useIntl } from 'gatsby-plugin-intl';

import Header from '../components/Header'
import Footer from '../components/Footer'

import { graphql } from "gatsby"

Modal.setAppElement('#___gatsby');

const modalStyles = {
  overlay: {
    backgroundColor: "rgba(0, 0, 0, 0.58)",
  },
  content: {
    position: "relative",
    top: "auto",
    left: "auto",
    right: "auto",
    bottom: "auto",
    maxWidth: "960px",
    margin: "32px auto",
    padding: 0,
    border: 0,
    zIndex: 999
  },
};

class TeamPage extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      modalOpen: false,
      currentMember: null,
      members: this.props.data.site.siteMetadata.members
    }

    this.openModal = this.openModal.bind(this)
    this.closeModal = this.closeModal.bind(this)
  }

  componentDidMount() {
    console.log(this.state.members)
    const memberHashed = this.state.members.filter(item => item.hash == this.props.location.hash)
    if(memberHashed.length > 0) {
      this.openModal(memberHashed[0][this.props.intl.locale])
    }
  }

  openModal(member) {
    this.setState({
      currentMember: member,
      modalOpen: true
    })
  }

  closeModal() {
    this.setState({modalOpen: false})
  };

  render() {
    return (
      <React.Fragment>
        <SEO
          title={this.props.intl.formatMessage({ id: "seo.team_title" })}
          description={this.props.intl.formatMessage({ id: "seo.team_description" })}
          lang={this.props.intl.locale}
        />
        <Header />
        <main id="app">
          <div className="container">
            <h1 className="text-center my-12"><FormattedHTMLMessage id="team_page.title" /></h1>
  
            {/* <p className="text-center font-half mb-16"><FormattedHTMLMessage id="team_page.subtitle" /></p> */}
  
            <div className="columns">
              {
                this.state.members.map((member, index) => {
                  return (
                    <button className="card-person column col-4 col-md-6 col-xs-12 mb-3 cursor-pointer" onClick={() => this.openModal(member[this.props.intl.locale])} onKeyUp={() => {}} key={`member_${index}`}>
                      <div className="card">
                          <div className="card-image" style={{ backgroundColor: `${member[this.props.intl.locale].color}` }}>
                              <div className="team-img" style={{backgroundImage: `url(${member[this.props.intl.locale].photo})`}}></div>
                          </div>
                          <div className="card-header">
                              <div className="card-title h5 text-center">{member[this.props.intl.locale].name}</div>
                              <div className="card-subtitle h5 text-center">{member[this.props.intl.locale].title}</div>
                          </div>
                      </div>
                    </button>
                  )
                })
              }
            </div>
        </div>
        </main>
        <Footer />

        <Modal
          isOpen={this.state.modalOpen}
          onRequestClose={this.closeModal}
          style={modalStyles}
          contentLabel="Modal"
          closeTimeoutMS={500}
        >
          {
            this.state.currentMember != null ? (
              <div>
                <div className="modal_thumb__wrapper" style={{backgroundColor: this.state.currentMember.color}}>
                  <img src={this.state.currentMember.photo} className="team_photo__big" alt={this.state.currentMember.name} />
                </div>
                <div className="modal__body">
                  <button className="model__close" onClick={this.closeModal}><i class="icon icon-cross"></i></button>
                  <div className="modal__name">{this.state.currentMember.name}</div>
                  <div className="modal__title">{this.state.currentMember.details.title}</div>

                  {
                    this.state.currentMember.details.links.map((link, index) => {
                      return (
                        <a target="_blank" rel="noreferrer" href={link.url} key={`link_${index}`} ><img src={link.icon} className="member_link" alt={this.state.currentMember.name} /></a>
                      )
                    })
                  }

                  <div>
                  {
                    this.state.currentMember.contacts ? (
                      <a href={this.state.currentMember.contacts} className="btn mt-3" target="_blank"><FormattedHTMLMessage id="save_contacts" /></a>
                    ) : null
                  }
                  </div>

                  <div className="modal__about">{this.state.currentMember.details.about}</div>
                </div>
              </div>
            ) : ''
          }
        </Modal>
      </React.Fragment>
    )
  }
}

export const query = graphql`
  query {
    site {
      siteMetadata {
        members {
          hash
          en {
            name
            title
            photo
            color
            contacts
            details {
              title
              links {
                icon
                url
              }
              about
            }
          }
          ru {
            name
            title
            photo
            color
            contacts
            details {
              title
              links {
                icon
                url
              }
              about
            }
          }
        }
      }
    }
  }
`

const SuperTeamPage = (props) => {
  const intl = useIntl();

  return (
    <TeamPage intl={intl} {...props} />
  )
}

export default SuperTeamPage
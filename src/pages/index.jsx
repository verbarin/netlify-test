import React from "react"
import { Link, FormattedHTMLMessage, useIntl } from 'gatsby-plugin-intl';
import Layout from "../components/layout"
import SEO from "../components/seo"
import Fullscreen from "react-full-screen";
import Marq from "../components/marq";
import ScrollDetecter from '../components/ScrollDetecter'
import HeroAnimatedBird from '../components/HeroAnimatedBird'
import HeroAnimatedHand from '../components/HeroAnimatedHand'
import * as EmailValidator from 'email-validator';import Modal from 'react-modal';
import { connect } from 'react-redux';
import { useAuth0 } from '@auth0/auth0-react';

import { useStaticQuery, graphql } from "gatsby"

import Hero1 from '../assets/svg/hand_1.svg'

import Ill1 from '../assets/svg/hero_main_1.svg'
import Ill2 from '../assets/svg/hero_main_2.svg'
import Ill3 from '../assets/svg/hero_main_3.svg'
import Ill4 from '../assets/svg/hero_main_4.svg'
import Ill5 from '../assets/svg/hero_main_5.svg'
import axios from "axios";

Modal.setAppElement('#___gatsby');

const modalStyles = {
  overlay: {
    backgroundColor: "rgba(0, 0, 0, 0.58)",
  },
  content: {
    position: "relative",
    top: "auto",
    left: "auto",
    right: "auto",
    bottom: "auto",
    maxWidth: "480px",
    margin: "32px auto",
    padding: 0,
    border: 0,
    zIndex: 999
  },
};

class HomeIndex extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      muted1: true,
      muted2: true,
      isDetectOne: false,
      isDetectTwo: false,
      isDetectThree: false,
      isDetectFour: false,
      isDetectFive: false,
      first_name: '',
      email: '',
      modalOpen: false,
      button_clicked: '',
      isSending: false,
      clickId: 0,
      agree_box: false
    }

    this.videoRef1 = React.createRef()
    this.videoRef2 = React.createRef()

    this.toggleVideo1 = this.toggleVideo1.bind(this)
    this.toggleVideo2 = this.toggleVideo2.bind(this)

    this.detectOne = this.detectOne.bind(this)
    this.detectTwo = this.detectTwo.bind(this)
    this.detectThree = this.detectThree.bind(this)
    this.detectFour = this.detectFour.bind(this)
    this.detectFive = this.detectFive.bind(this)
    this.exitHandler = this.exitHandler.bind(this)

    this.onChange = this.onChange.bind(this)
    this.openModal = this.openModal.bind(this)
    this.closeModal = this.closeModal.bind(this)
    this.sendDemo = this.sendDemo.bind(this)
    this.renderError = this.renderError.bind(this)
    this.validatingForm = this.validatingForm.bind(this)
  }

  validatingForm() {
    if(this.state.first_name.trim().length < 2) {
        this.setState({
            error: {
                title: 'first_name',
                string: this.props.intl.formatMessage({ id: "validation.first_name" })
            },
            isSending: true
        })
        return false;
    }
    if(!EmailValidator.validate(this.state.email)) {
      this.setState({
          error: {
              title: 'email',
              string: this.props.intl.formatMessage({ id: "validation.email" })
          },
          isSending: true
      })
      return false;
  }
    if(!this.state.agree_box === true) {
        this.setState({
            error: {
                title: 'agree_box',
                string: this.props.intl.formatMessage({ id: "validation.agree" })
            },
            isSending: true
        })
        return false;
    }

    this.setState({
        error: null,
        isSending: false
    })
}

  sendDemo(id) {
    if(!this.state.agree_box === true) {
      return false;
    }
    if(!EmailValidator.validate(this.state.email)) {
      return false;
    }
    if(this.state.first_name.trim().length > 1) {
      this.setState({isSending: true})
      axios.post(this.props.data.site.siteMetadata.endpoints.log + this.props.token, {
        type_id: id,
        data: {
          button_clicked: 'free_pdf',
          email: this.state.email,
          first_name: this.state.first_name,
          term: this.state.term
        },
        timestamp: new Date().toISOString(),
        lang: this.props.intl.locale
      }, {
        headers: {
          "Content-Type": "application/json",
          "Accept": "application/json",
        }
      })
        .then(response => {
          this.closeModal()
          this.setState({isSending: false})
          window.location = 'https://app.upfinity.io/';
        })
        .catch(e => {
          console.log(e)
          this.setState({isSending: false})
        })
        .then(() => {
          this.closeModal()
          this.setState({isSending: false})
        })
    }
  }

  onChange(e) {
    if(e.target.type == 'checkbox') {
      this.setState({
        [e.target.name]: e.target.checked
      }, () => {
        this.validatingForm()
      })
    } else {
      this.setState({
        [e.target.name]: e.target.value
      }, () => {
        this.validatingForm()
      })
    }
  }

  openModal(type) {
    this.setState({
      button_clicked: type,
      clickId: type,
      modalOpen: true
    }, () => {
      this.validatingForm()
    })
  }

  closeModal() {
    this.setState({modalOpen: false})
  };
  
  toggleVideo1() {
    this.setState({
      muted1: !this.state.muted1
    }, () => {
      if(!this.state.muted1) this.videoRef1.current.currentTime = 0
    })
  }

  toggleVideo2() {
    this.setState({
      muted2: !this.state.muted2
    }, () => {
      if(!this.state.muted2) this.videoRef2.current.currentTime = 0
    })
  }

  detectOne() {
    this.setState({
      isDetectOne: true
    })
  }

  detectTwo() {
    this.setState({isDetectTwo: true})
  }

  detectThree() {
    this.setState({isDetectThree: true})
  }

  detectFour() {
    this.setState({isDetectFour: true})
  }

  detectFive() {
    this.setState({isDetectFive: true})
  }

  componentDidUpdate(prevProps) {
    if(JSON.stringify(this.props.auth) !== JSON.stringify(prevProps.auth)) {
        if(this.props.auth && this.props.auth.user && this.props.auth.user.email) {
            this.setState({
                email: this.props.auth.user.email
            })
        }

        if(this.props.auth && this.props.auth.user && this.props.auth.user.given_name) {
            this.setState({
                first_name: this.props.auth.user.given_name
            })
        }
    }
}

  shouldComponentUpdate(prevState, nextState) {
    if(prevState.muted1 != nextState.muted1) return true
    if(prevState.muted2 != nextState.muted2) return true

    return false
  }

  exitHandler() {
    console.log(this.state.muted1, this.state.muted2)
    if(!this.state.muted1 || !this.state.muted2) {
      if(!document.webkitIsFullScreen && !document.mozFullScreen && !document.msFullscreenElement) {
        this.setState({
          muted1: true,
          muted2: true
        })
      }
    }
  }

  componentDidMount() {
    if (document.addEventListener) {
        document.addEventListener('fullscreenchange', this.exitHandler, false);
        document.addEventListener('mozfullscreenchange', this.exitHandler, false);
        document.addEventListener('MSFullscreenChange', this.exitHandler, false);
        document.addEventListener('webkitfullscreenchange', this.exitHandler, false);
    }

    if(this.props.auth && this.props.auth.user && this.props.auth.user.email) {
      this.setState({
          email: this.props.auth.user.email
      })
    }

    if(this.props.auth && this.props.auth.user && this.props.auth.user.given_name) {
        this.setState({
            first_name: this.props.auth.user.given_name
        })
    }
  }

  componentWillUnmount() {
    document.removeEventListener('fullscreenchange', this.exitHandler, false);
    document.removeEventListener('mozfullscreenchange', this.exitHandler, false);
    document.removeEventListener('MSFullscreenChange', this.exitHandler, false);
    document.removeEventListener('webkitfullscreenchange', this.exitHandler, false);
  }

  renderError(title) {
    if (this.state.error && this.state.error.title === title) {
        return <small>{ this.state.error.string }</small>
    } else {
        return null
    }
}

goPreferences = () => {
  window.location = process.env.GATSBY_PREFERENCES_LINK;
}

  render() {
    return (
      <Layout location={this.props.location} title="Main page">
        <SEO
          title={this.props.intl.formatMessage({ id: "seo.index_title" })}
          description={this.props.intl.formatMessage({ id: "seo.index_description" })}
          lang={this.props.intl.locale}
        />

        <div className="hero_custom">
          <HeroAnimatedHand>
            <Hero1 className="hero_1" />
          </HeroAnimatedHand>
          <HeroAnimatedBird />
        </div>

        <div className="container mt-24 pt-12 pb-12 position-relative mt-0-mobile">
          <h1 className="main_hero text-center"><FormattedHTMLMessage id="home.hero_1" /></h1>
  
          <div className="subtitle"><FormattedHTMLMessage id="home.hero_2" /></div>

          <div className="text-center">
            <button onClick={() => this.openModal(4)} className="btn btn-xl btn-primary mt-12"><FormattedHTMLMessage id="home.cta" /></button>
            {/* <a target="_blank" href={this.props.data.site.siteMetadata.endpoints.soup} className="btn btn-xl btn-primary mt-12"><FormattedHTMLMessage id="home.cta" /></a> */}
          </div>
        </div>
  
        <div className="mx-3">
          <span className="hide-1122">
            <button onClick={this.toggleVideo1} className={"card-person p-0 mt-24 " + (!this.state.muted1 ? '' : 'main-video_wrapper')}>
              <Fullscreen tabIndex={0} enabled={!this.state.muted1}>
                <video tabIndex={0} ref={this.videoRef1} className="main-video" src={`../../denis_video_${this.props.intl.locale}.mp4`} autoPlay={true} muted={this.state.muted1} loop>
                  <track kind="captions" />
                </video>
              </Fullscreen>
            </button>
          </span>
          <span className="display-1122">
            <button onClick={this.toggleVideo2} className={"card-person p-0 mt-24 " + (!this.state.muted2 ? '' : 'main-video_wrapper')}>
              <Fullscreen tabIndex={0} enabled={!this.state.muted2}>
                <video tabIndex={0} ref={this.videoRef2} className="main-video" src={`../../denis_video_vertical_${this.props.intl.locale}.mp4`} autoPlay={true} muted={this.state.muted2} loop>
                  <track kind="captions" />
                </video>
              </Fullscreen>
            </button>
          </span>
        </div>

        <h2 className="big-header mt-24 mb-12"><FormattedHTMLMessage id="home.marq_title" /></h2>

        <Marq
          rows={7}
          items={[
            {id: 1, label: this.props.intl.formatMessage({ id: "home.items.agro" }), count: 8455},
            {id: 2, label: this.props.intl.formatMessage({ id: "home.items.apps" }), count: 37856},
            {id: 3, label: this.props.intl.formatMessage({ id: "home.items.machine" }), count: 19641},
            {id: 4, label: this.props.intl.formatMessage({ id: "home.items.bio" }), count: 21990},
            {id: 5, label: this.props.intl.formatMessage({ id: "home.items.fashion" }), count: 16368},
            {id: 6, label: this.props.intl.formatMessage({ id: "home.items.ecommerce" }), count: 89329},
            {id: 7, label: this.props.intl.formatMessage({ id: "home.items.retail" }), count: 89329},
            {id: 8, label: this.props.intl.formatMessage({ id: "home.items.lifestyle" }), count: 35307},
            {id: 9, label: this.props.intl.formatMessage({ id: "home.items.electronics" }), count: 39185},
            {id: 10, label: this.props.intl.formatMessage({ id: "home.items.goods" }), count: 22112},
            {id: 11, label: this.props.intl.formatMessage({ id: "home.items.publishing" }), count: 32383},
            {id: 12, label: this.props.intl.formatMessage({ id: "home.items.data" }), count: 49958},
            {id: 13, label: this.props.intl.formatMessage({ id: "home.items.design" }), count: 54985},
            {id: 14, label: this.props.intl.formatMessage({ id: "home.items.edu" }), count: 36306},
            {id: 15, label: this.props.intl.formatMessage({ id: "home.items.energy" }), count: 22435},
            {id: 16, label: this.props.intl.formatMessage({ id: "home.items.event" }), count: 11398},
            {id: 17, label: this.props.intl.formatMessage({ id: "home.items.fintech" }), count: 72697},
            {id: 18, label: this.props.intl.formatMessage({ id: "home.items.insura" }), count: 72697},
            {id: 19, label: this.props.intl.formatMessage({ id: "home.items.food" }), count: 30148},
            {id: 20, label: this.props.intl.formatMessage({ id: "home.items.gaming" }), count: 12624},
            {id: 21, label: this.props.intl.formatMessage({ id: "home.items.health" }), count: 79738},
            {id: 22, label: this.props.intl.formatMessage({ id: "home.items.it" }), count: 141661},
            {id: 23, label: this.props.intl.formatMessage({ id: "home.items.infra" }), count: 117051},
            {id: 24, label: this.props.intl.formatMessage({ id: "home.items.social" }), count: 89317},
            {id: 25, label: this.props.intl.formatMessage({ id: "home.items.manufac" }), count: 69400},
            {id: 26, label:  this.props.intl.formatMessage({ id: "home.items.music" }), count: 10144},
            {id: 27, label: this.props.intl.formatMessage({ id: "home.items.video" }), count: 18198},
            {id: 28, label: this.props.intl.formatMessage({ id: "home.items.cyber" }), count: 19402},
            {id: 29, label: this.props.intl.formatMessage({ id: "home.items.real" }), count: 46233},
            {id: 30, label: this.props.intl.formatMessage({ id: "home.items.marketing" }), count: 93331},
            {id: 31, label: this.props.intl.formatMessage({ id: "home.items.sports" }), count: 19130},
            {id: 32, label: this.props.intl.formatMessage({ id: "home.items.engineer" }), count: 66156},
            {id: 33, label: this.props.intl.formatMessage({ id: "home.items.travel" }), count: 19820}
          ]}
        ></Marq>

        <div className="big-header"><FormattedHTMLMessage id="home.why" /></div>

        <div className="container">
          <ScrollDetecter detected={this.detectOne}>
            <div className={"hero_wrapper " + (this.state.isDetectOne ? 'hero_animated' : '')}>
              <div className="hero_text">
                <Ill1 className="hero_svg_1" />
                <div className="hero_title"><FormattedHTMLMessage id="home.block_1_title" /></div>
                <div className="hero_about"><FormattedHTMLMessage id="home.block_1_body" />,</div>
              </div>
            </div>
          </ScrollDetecter>

          <ScrollDetecter detected={this.detectTwo}>
            <div className={"hero_wrapper_2 " + (this.state.isDetectTwo ? 'hero_animated' : '')}>
              <div className="hero_text">
                <Ill2 className="hero_svg_2" />
                <div className="hero_title"><FormattedHTMLMessage id="home.block_2_title" /></div>
                <div className="hero_about"><FormattedHTMLMessage id="home.block_2_body" />,</div>
              </div>
            </div>
          </ScrollDetecter>

          <ScrollDetecter detected={this.detectThree}>
            <div className={"hero_wrapper_3 " + (this.state.isDetectThree ? 'hero_animated' : '')}>
              <div className="hero_text">
                <Ill3 className="hero_svg_3" />
                <div className="hero_title"><FormattedHTMLMessage id="home.block_3_title" /></div>
                <div className="hero_about"><FormattedHTMLMessage id="home.block_3_body" />,</div>
              </div>
            </div>
          </ScrollDetecter>

          <ScrollDetecter detected={this.detectFour}>
            <div className={"hero_wrapper_4 " + (this.state.isDetectFour ? 'hero_animated' : '')}>
              <div className="hero_text">
                <Ill4 className="hero_svg_4" />
                <div className="hero_title"><FormattedHTMLMessage id="home.block_4_title" /></div>
                <div className="hero_about"><FormattedHTMLMessage id="home.block_4_body" />,</div>
              </div>
            </div>
          </ScrollDetecter>

          <ScrollDetecter detected={this.detectFive}>
            <div className={"hero_wrapper_5 " + (this.state.isDetectFive ? 'hero_animated' : '')}>
              <div className="hero_text">
                <Ill5 className="hero_svg_5" />
                <div className="hero_title"><FormattedHTMLMessage id="home.block_5_title" /></div>
                <div className="hero_about"><FormattedHTMLMessage id="home.block_5_body" />.</div>
              </div>
            </div>
          </ScrollDetecter>

          <div className="text-center mt-24">
            <div className="h1"><FormattedHTMLMessage id="home.block_precta" /></div>
            <button onClick={() => this.goPreferences(5)} className="btn btn-xl btn-primary mt-12"><FormattedHTMLMessage id="home.block_cta" /></button>
            {/* <a target="_blank" href={this.props.data.site.siteMetadata.endpoints.soup} className="btn btn-xl btn-primary mt-12"><FormattedHTMLMessage id="home.block_cta" /></a> */}
          </div>
        </div>

        <Modal
          isOpen={this.state.modalOpen}
          onRequestClose={this.closeModal}
          style={modalStyles}
          contentLabel="Modal"
          closeTimeoutMS={500}
        >
          <div className="modal__body">
            <button className="model__close" onClick={this.closeModal}><i class="icon icon-cross"></i></button>
            <div className="modal__name"><FormattedHTMLMessage id="home.try_demo" /></div>
            <div className="form-group mt-5">
              <label className="form-label"><FormattedHTMLMessage id="modal.name" /></label>
              <input className="form-input" value={this.state.first_name} onChange={this.onChange} name="first_name" type="text" placeholder={this.props.intl.formatMessage({id: `modal.name`})} />
              {this.renderError('first_name')}
            </div>
            <div className="form-group mb-5">
              <label className="form-label">Email</label>
              <input className="form-input" value={this.state.email} onChange={this.onChange} name="email" type="text" placeholder="Email" />
              {this.renderError('email')}
            </div>

            <div>
              <label className="form-radio mr-5 label-static">
                <input type="checkbox" checked={this.state.agree_box === true} value={this.state.agree_box === true} name="agree_box" onChange={this.onChange} />
                <i className="form-icon"></i> <FormattedHTMLMessage id="agree" /> <u><a href="/terms" target="_blank"><FormattedHTMLMessage id="terms" /></a></u></label>
                {this.renderError('agree_box')}
            </div>

            <button disabled={this.state.isSending} className="btn btn-primary font-bold px-6 my-5" onClick={() => this.sendDemo(this.state.clickId)}><FormattedHTMLMessage id="modal.button_text_pdf" /></button>
          </div>
        </Modal>
  
      </Layout>
    )
  }
}

const SuperHome = (props) => {
  const intl = useIntl();
  const auth = useAuth0();

  const data = useStaticQuery(graphql`
        query {
            site {
                siteMetadata {
                    endpoints {
                        signin
                        signup
                        purchase
                        subscribe
                        enterprise
                        soup
                        free_pdf
                        log
                    }
                }
            }
        }
    `)

    props = {
        ...props,
        data
    }

  return (
      <HomeIndex intl={intl} {...props} auth={auth} />
  )
}

const mapStateToProps = ({ token }) => {
  return { token }
}

const mapDispatchToProps = dispatch => {
  return {}
}

export default connect(mapStateToProps, mapDispatchToProps)(SuperHome)

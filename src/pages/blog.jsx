import React, { useState } from "react"
import Header from '../components/Header'
import Footer from '../components/Footer'
import { Link, useIntl } from 'gatsby-plugin-intl';
import SEO from "../components/seo"
import { graphql } from "gatsby"

const BlogPage = ({ data, pageContext }) => {
  const allposts = data.allMarkdownRemark.nodes
  const posts = data.allMarkdownRemark.nodes
  let queryPosts = []

  const intl = useIntl()

  if (posts.length === 0) {
    return (
      <React.Fragment>
        <Header />
        <main id="app">
          <div className="container">
            <h1 className="text-center my-12">{ intl.formatMessage({ id: "blog_page.title" }) }</h1>

            <p>...</p>
          </div>
        </main>
        <Footer />
      </React.Fragment>
    )
  }

  const emptyQuery = ""

  const [state, setState] = useState({
    filteredData: [],
    query: emptyQuery,
  })

  const flushQuery = () => {
    setState({
      query: ""
    })
  }

  const handleSearch = event => {
    const queryOriginal = event.target.value
    const query = queryOriginal.trim()

    const filteredData = posts.filter(post => {
      const { description, title, tags } = post.frontmatter
      return (
        description.toLowerCase().includes(queryOriginal.toLowerCase()) ||
        title.toLowerCase().includes(queryOriginal.toLowerCase()) ||
        (tags && tags
          .join("")
          .toLowerCase()
          .includes(queryOriginal.toLowerCase()))
      )
    })
    setState({
      query: queryOriginal,
      filteredData,
    })
  }

  const { filteredData, query } = state
  const hasSearchResults = filteredData && query !== emptyQuery

  queryPosts = hasSearchResults ? filteredData : queryPosts

  return (
    <React.Fragment>
      <SEO
        title={intl.formatMessage({ id: "seo.blog_title" })}
        description={intl.formatMessage({ id: "seo.blog_description" })}
        lang={intl.locale}
      />
      <Header />
      <main id="app">
        <div className="container">
          <h1 className="text-center my-12">{ intl.formatMessage({ id: "blog_page.title" }) }</h1>

          <div className="columns justify-content-end">
            <div className="column col-4 col-md-12 mb-5">
              <div className="form-group">
                <div className="input-group">
                  <input className="form-input" type="text" onChange={handleSearch} value={state.query} />
                  <button onClick={flushQuery} className="btn btn-primary input-group-btn">{ intl.formatMessage({ id: "blog_page.cancel" }) }</button>
                </div>
              </div>
            </div>
          </div>

          {
            pageContext.allCategories.map((category, index) => {
              return (
                  category.locale === intl.locale ? (
                    <span key={`category_${index}`} className="chip"><Link to={`/blog/category/${category.slug}`}>{intl.formatMessage({ id: category.slug })}</Link></span>
                  ) : null
              )
            })
          }
         
         {
           queryPosts.length > 0 ? (
            <div className="columns mt-6">
            {
              queryPosts.map((post, index) => {
                const thumbReady = post.frontmatter.thumbnail.replace('/static', '');

                return (
                    <div key={`article_${index}_${post.fields.slug}`} data-attr={post.fields.slug} className="column col-4 col-sm-12 col-md-6 mb-4">
                      <article className="blog-card">
                        <Link to={`/blog/article${post.fields.slug}`}>
                          <div className="img-background" style={{backgroundImage: `url(${thumbReady})`}}></div>

                          <header><h3 className="h4">
                            {post.frontmatter.title}
                            </h3></header>
                          

                          <section><p>{post.frontmatter.description}</p></section>
                        </Link>

                        <div className="tags-wrapper">
                          {
                            post.frontmatter.tags ? (
                              post.frontmatter.tags.map((tag, index) => {
                                return (
                                  <span key={`tag_${index}`}>#{tag}</span>
                                )
                              })
                            ) : ('')
                          }
                        </div>
                      </article>
                    </div>
                )
              })
            }
          </div>
           ) : (
            <div className="columns mt-6">
            {
              posts.map((post, index) => {
                const thumbReady = post.frontmatter.thumbnail.replace('/static', '');

                return (
                  intl.locale === post.frontmatter.locale ? 
                  (
                    <div key={`article_${index}_${post.fields.slug}`} data-attr={post.fields.slug} className="column col-4 col-sm-12 col-md-6 mb-4">
                      <article className="blog-card">
                      <Link to={`/blog/article${post.fields.slug}`}>
                        <div className="img-background" style={{backgroundImage: `url(${thumbReady})`}}></div>

                        <header><h3 className="h4">
                          {post.frontmatter.title}
                          </h3>
                        </header>
                        

                        <section><p>{post.frontmatter.description}</p></section>
                        </Link>

                        <div className="tags-wrapper">
                          {
                            post.frontmatter.tags ? (
                              post.frontmatter.tags.map((tag, index) => {
                                return (
                                  <span key={`tag_${index}`}>#{tag}</span>
                                )
                              })
                            ) : ('')
                          }
                        </div>
                      </article>
                    </div>
                  ) : ('')
                )
              })
            }

            {
              pageContext.numPages > 1 ? (
                <div className="column col-12 d-flex mt-6 justify-content-center">
                  <ul className="pagination">
                    
                    {
                      pageContext.currentPage == 1 && (
                        <li className="page-item disabled">
                          <Link to={`/blog/page/${pageContext.currentPage - 1}`}>{ intl.formatMessage({ id: "previous" }) }</Link>
                        </li>
                      )
                    }

                    {
                      pageContext.currentPage == 2 && (
                        <li className="page-item">
                          <Link to={`/blog`}>{ intl.formatMessage({ id: "previous" }) }</Link>
                        </li>
                      )
                    }

                    {
                      pageContext.currentPage != 1 && pageContext.currentPage != 2 && (
                        <li className="page-item">
                          <Link to={`/blog/page/${pageContext.currentPage - 1}`}>{ intl.formatMessage({ id: "previous" }) }</Link>
                        </li>
                      )
                    }
                    
                    <li className="page-item">
                      <span>{ pageContext.currentPage } / { pageContext.numPages }</span>
                    </li>

                    {
                      pageContext.currentPage >= pageContext.numPages && (
                        <li className="page-item disabled">
                          <Link to={`/blog/page/${pageContext.currentPage + 1}`}>{ intl.formatMessage({ id: "next" }) }</Link>
                        </li>
                      )
                    }

                    {
                      pageContext.currentPage < pageContext.numPages && (
                        <li className="page-item">
                          <Link to={`/blog/page/${pageContext.currentPage + 1}`}>{ intl.formatMessage({ id: "next" }) }</Link>
                        </li>
                      )
                    }
                  </ul>
                </div>
              ) : null
            }
            
          </div>
           )
         }

          <h3>{ intl.formatMessage({ id: "blog_page.tags" }) }</h3>
          {
            pageContext.allTags.map((tag, index) => {
              return (
                <span key={`tag_${index}`} className="chip"><Link to={`/blog/tag/${tag.slug}`}>#{tag.title}</Link></span>
              )
            })
          }
        </div>
      </main>
      <Footer />
    </React.Fragment>
  )
}

export const pageQuery = graphql`
  query blogPostsList($skip: Int!, $limit: Int!) {
    site {
      siteMetadata {
        title
      }
    }
    allMarkdownRemark(
      sort: { fields: [frontmatter___date], order: DESC },
      skip: $skip,
      limit: $limit
    ) {
      nodes {
        excerpt
        fields {
          slug
          locale
        }
        frontmatter {
          date
          title
          description
          locale
          thumbnail
          tags
        }
      }
    }
  }
`

export default BlogPage
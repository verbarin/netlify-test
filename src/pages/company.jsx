import React from "react"
import Header from './../components/Header'
import Footer from './../components/Footer'
import SEO from "../components/seo"
import { Link } from 'gatsby-plugin-intl';
import { FormattedHTMLMessage, useIntl } from "gatsby-plugin-intl";
import Modal from 'react-modal';
import axios from "axios";
import * as EmailValidator from 'email-validator';
import { connect } from 'react-redux';
import { useAuth0 } from '@auth0/auth0-react';

import { useStaticQuery, graphql } from "gatsby";

Modal.setAppElement('#___gatsby');

const modalStyles = {
  overlay: {
    backgroundColor: "rgba(0, 0, 0, 0.58)",
  },
  content: {
    position: "relative",
    top: "auto",
    left: "auto",
    right: "auto",
    bottom: "auto",
    maxWidth: "480px",
    margin: "32px auto",
    padding: 0,
    border: 0,
    zIndex: 999
    //agree_box: false
  }
};

class CompanyPage extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      first_name: '',
      email: '',
      button_clicked: '',
      isSending: false,
      agree_box: false
    }

    this.onChange = this.onChange.bind(this)
    this.sendDemo = this.sendDemo.bind(this)
    this.openModal = this.openModal.bind(this)
    this.closeModal = this.closeModal.bind(this)
    this.validatingForm = this.validatingForm.bind(this)
    this.renderError = this.renderError.bind(this)
  }

  validatingForm() {
    if(this.state.first_name.trim().length < 2) {
        this.setState({
            error: {
                title: 'first_name',
                string: this.props.intl.formatMessage({ id: "validation.first_name" })
            },
            isSending: true
        })
        return false;
    }
    if(!EmailValidator.validate(this.state.email)) {
        this.setState({
            error: {
                title: 'email',
                string: this.props.intl.formatMessage({ id: "validation.email" })
            },
            isSending: true
        })
        return false;
    }
    if(!this.state.agree_box === true) {
        this.setState({
            error: {
                title: 'agree_box',
                string: this.props.intl.formatMessage({ id: "validation.agree" })
            },
            isSending: true
        })
        return false;
    }

    this.setState({
        error: null,
        isSending: false
    })
}

  sendDemo() {
    if(!this.state.agree_box === true) {
      return false;
    }
    if(!EmailValidator.validate(this.state.email)) {
        return false;
    }
    if(this.state.first_name.trim().length > 1) {
      this.setState({isSending: true})
      axios.post(this.props.data.site.siteMetadata.endpoints.log + this.props.token, {
        type_id: 11,
        data: {
          button_clicked: 'about_company_mission_bottom',
          email: this.state.email,
          first_name: this.state.first_name,
        },
        timestamp: new Date().toISOString(),
        lang: this.props.intl.locale
      }, {
        headers: {
          "Content-Type": "application/json",
          "Accept": "application/json",
        }
      })
        .then(response => {
          this.closeModal()
          this.setState({isSending: false})
          window.location = process.env.GATSBY_PREFERENCES_LINK;
        })
        .catch(e => {
          console.log(e)
          this.setState({isSending: false})
        })
        .then(() => {
          this.closeModal()
          this.setState({isSending: false})
        })
    }
  }

  goPreferences = () => {
    window.location = process.env.GATSBY_PREFERENCES_LINK;
  }

  onChange(e) {
    if(e.target.type == 'checkbox') {
      this.setState({
        [e.target.name]: e.target.checked
      }, () => {
        this.validatingForm()
      })
    } else {
      this.setState({
        [e.target.name]: e.target.value
      }, () => {
        this.validatingForm()
      })
    }
  }

  openModal() {
    this.setState({
      button_clicked: 'mission',
      modalOpen: true
    }, () => {
      this.validatingForm()
    })
  }

  closeModal() {
    this.setState({modalOpen: false})
  };

  renderError(title) {
    if (this.state.error && this.state.error.title === title) {
        return <small>{ this.state.error.string }</small>
    } else {
        return null
    }
}

componentDidMount() {
  if(this.props.auth && this.props.auth.user && this.props.auth.user.email) {
    this.setState({
        email: this.props.auth.user.email
    })
  }

  if(this.props.auth && this.props.auth.user && this.props.auth.user.given_name) {
      this.setState({
          first_name: this.props.auth.user.given_name
      })
  }
}

componentDidUpdate(prevProps) {
  if(JSON.stringify(this.props.auth) !== JSON.stringify(prevProps.auth)) {
      if(this.props.auth && this.props.auth.user && this.props.auth.user.email) {
          this.setState({
              email: this.props.auth.user.email
          })
      }

      if(this.props.auth && this.props.auth.user && this.props.auth.user.given_name) {
          this.setState({
              first_name: this.props.auth.user.given_name
          })
      }
  }
}
  
  render() {    
    return (
      <React.Fragment>
        <SEO
          title={this.props.intl.formatMessage({ id: "seo.company_title" })}
          description={this.props.intl.formatMessage({ id: "seo.company_description" })}
          lang={this.props.intl.locale}
        />
        <Header />
        <main id="app">
          <div className="container">
            {/* 
  
            
  
            <div className="h3 company_2 mt-12 text-right line-1"><FormattedHTMLMessage id="company_page.subsubtitle" /></div> */}
  
            {/* <div className="h1 text-center line-1 mt-12 mb-12"><FormattedHTMLMessage id="company_page.about" /></div> */}
  
            <div className="h1 company_1 line-1 my-12"><FormattedHTMLMessage id="company_page.subtitle" /></div>
  
            <h1 className="text-center my-12"><FormattedHTMLMessage id="company_page.about" /></h1>
  
            <div className="animated_wrapper">
              <img src="../../svg_1.svg" className="animated_svg_1" alt=""/>
              <div className="animated_text">
                <div className="animated_title text-primary"><FormattedHTMLMessage id="company_page.block_1_title" /></div>
                <div className="animated_about"><FormattedHTMLMessage id="company_page.block_1_body" />,</div>
              </div>
            </div>
  
            <div className="animated_wrapper_2">
              <img src="../../svg_2.svg" className="animated_svg_2" alt=""/>
              <img src="../../svg_3.svg" className="animated_svg_3" alt=""/>
              <div className="animated_text">
                <div className="animated_title text-primary"><FormattedHTMLMessage id="company_page.block_2_title" /></div>
                <div className="animated_about"><FormattedHTMLMessage id="company_page.block_2_body" />,</div>
              </div>
            </div>
  
            <div className="animated_wrapper_3">
              <img src="../../svg_4.svg" className="animated_svg_4" alt=""/>
              <div className="animated_text">
                <div className="animated_title text-primary"><FormattedHTMLMessage id="company_page.block_3_title" /></div>
                <div className="animated_about"><FormattedHTMLMessage id="company_page.block_3_body" />,</div>
              </div>
            </div>
  
            <div className="animated_wrapper_4">
              <img src="../../svg_5.svg" className="animated_svg_6" alt=""/>
              <div className="animated_text">
                <div className="animated_title text-primary"><FormattedHTMLMessage id="company_page.block_4_title" /></div>
                <div className="animated_about"><FormattedHTMLMessage id="company_page.block_4_body" />.</div>
              </div>
            </div>
  
            <div className="text-center mt-24">
              <div className="h1"><FormattedHTMLMessage id="home.block_precta" /></div>
              {/* <a target="_blank" href={data.site.siteMetadata.endpoints.soup} className="btn btn-xl btn-primary mt-12"><FormattedHTMLMessage id="home.block_cta" /></a> */}
              <button onClick={this.goPreferences} className="btn btn-xl btn-primary mt-12"><FormattedHTMLMessage id="home.block_cta" /></button>
            </div>
          </div>
        </main>
        <Footer />
                   
        <Modal
            isOpen={this.state.modalOpen}
            onRequestClose={this.closeModal}
            style={modalStyles}
            contentLabel="Modal"
            closeTimeoutMS={500}
          >            
            <div className="modal__body">
              <div className="modal__name"><FormattedHTMLMessage id="home.try_demo" /></div>
              <div className="form-group mt-5">
                <label className="form-label" htmlFor="input-example-4"><FormattedHTMLMessage id="modal.name" /></label>
                <input className="form-input" value={this.state.first_name} onChange={this.onChange} name="first_name" type="text" id="input-example-4" placeholder={this.props.intl.formatMessage({id: `modal.name`})} />
                {this.renderError('first_name')}
              </div>
              <div className="form-group mb-5">
                <label className="form-label" htmlFor="input-example-4">Email</label>
                <input className="form-input" value={this.state.email} onChange={this.onChange} name="email" type="text" id="input-example-4" placeholder="Email" />
                {this.renderError('email')}
              </div>
  
              <div>
                <label className="form-radio mr-5 label-static">
                <input type="checkbox" checked={this.state.agree_box === true} value={this.state.agree_box === true} name="agree_box" onChange={this.onChange} />
                <i className="form-icon"></i> <FormattedHTMLMessage id="agree" /> <u><a href="/terms" target="_blank"><FormattedHTMLMessage id="terms" /></a></u></label>
                {this.renderError('agree_box')}
              </div>

              <button disabled={this.state.isSending} className="btn btn-primary font-bold px-6 my-5" onClick={this.sendDemo}><FormattedHTMLMessage id="modal.button_text_pdf" /></button>
            </div>
          </Modal>
      </React.Fragment>
    )
  }
}

// export const query = graphql`
// query {
//   site {
//       siteMetadata {
//           endpoints {
//               signin
//               signup
//               purchase
//               subscribe
//               enterprise
//               soup
//               free_pdf
//           }
//       }
//     }
//   }
// `

const SuperCompanyPage = (props) => {
  const intl = useIntl();
  const auth = useAuth0();

  const data = useStaticQuery(graphql`
        query {
            site {
                siteMetadata {
                    endpoints {
                      signin
                      signup
                      purchase
                      subscribe
                      enterprise
                      soup
                      free_pdf
                      log
                    }
                }
            }
        }
    `)

    props = {
        ...props,
        data
    }

  return (
      <CompanyPage intl={intl} {...props} auth={auth} />
  )
}

const mapStateToProps = ({ token }) => {
  return { token }
}

const mapDispatchToProps = dispatch => {
  return {}
}

export default connect(mapStateToProps, mapDispatchToProps)(SuperCompanyPage)